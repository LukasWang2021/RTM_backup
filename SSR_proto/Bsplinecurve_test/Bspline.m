%X:原始资料,d:控制点
%n:数据VP点数,k:B样条的次数
X=load('data1.txt');
Y=load('data2.txt');
[n,numy]=size(X); %得数据维数;
k=5;
%弦长参数化
u(k+1)=0;
for i=1:(n-1)
    u(k+i+1)=u(k+i)+abs(X(i+1,1)-X(i,1));
end
%====规范参数化=========================
temp=u(n+k);
for i=(k+1):(n+k-1) %(k+1):(n-1+k)足够了
    u(i)=1+u(i)/temp;
end
%首末节点重复k+1个 从0到1是开头 从2到3是结尾
for i=1:(k+1)
    u(i)=(i-1)/k;
    u(n-1+k+i)=2+(i-1)/k;
end
%% ===============================
%A:方程系数------采用递推算法-----------------------
A=zeros(n+k-1); % 从位置到速度减少一阶,节点数掐头去尾少2个

%轨迹初速度系数（边界条件1）
A(1,1)=-5*Bbase(k-3,k-1,u,u(k+1))/(u(k+2)-u(k-3));
A(1,2)=5*(Bbase(k-3,k-1,u,u(k+1))/(u(k+2 )-u(k-3))-Bbase(k-2,k-1,u,u(k+1))/(u(k+3)-u(k-2)));
A(1,3)=5*(Bbase(k-2,k-1,u,u(k+1))/(u(k+3)-u(k-2))-Bbase(k-1,k-1,u,u(k+1))/(u(k+4)-u(k-1)));
A(1,4)=5*(Bbase(k-1,k-1,u,u(k+1))/(u(k+4)-u(k-1))-Bbase(k,k-1,u,u(k+1))/(u(k+5)-u(k)));
A(1,5)=5*Bbase(k,k-1,u,u(k+1))/(u(k+5)-u(k));
%轨迹末速度系数（边界条件2）
A(n+k-1,n+k-5)=-5*Bbase(n+k-4,k-1,u,u(n+k))/(u(n+k+1)-u(n+k-4));
A(n+k-1,n+k-4)=5*(Bbase(n+k-4,k-1,u,u(n+k))/(u(n+k+1)-u(n+k-4))-Bbase(n+k-3,k-1,u,u(n+k))/(u(n+k+2)-u(n+k-3)));
A(n+k-1,n+k-3)=5*(Bbase(n+k-3,k-1,u,u(n+k))/(u(n+k+2)-u(n+k-3))-Bbase(n+k-2,k-1,u,u(n+k))/(u(n+k+3)-u(n+k-2)));
A(n+k-1,n+k-2)=5*(Bbase(n+k-2,k-1,u,u(n+k))/(u(n+k+3)-u(n+k-2))-Bbase(n+k-1,k-1,u,u(n+k))/(u(n+k+4)-u(n+k-1)));
A(n+k-1,n+k-1)=5*Bbase(n+k-1,k-1,u,u(n+k))/(u(n+k+4)-u(n+k-1));

%轨迹初加速度系数（边界条件3）
A(2,1)=20*Bbase(k-2,k-2,u,u(k+1))/((u(k+2)-u(k-2))*(u(k+2)-u(k-3)));
A(2,2)=20*Bbase(k-1,k-2,u,u(k+1))/((u(k+3)-u(k-1))*(u(k+3)-u(k-2)))-20*(Bbase(k-2,k-2,u,u(k+1))/(u(k+2)-u(k-2)))*(1/(u(k+3)-u(k-2))+1/(u(k+2)-u(k-3)));
A(2,3)=20*Bbase(k-2,k-2,u,u(k+1))/((u(k+2)-u(k-2))*(u(k+3)-u(k-2)))-20*(Bbase(k-1,k-2,u,u(k+1))/(u(k+3)-u(k-1)))*(1/(u(k+4)-u(k-1))+1/(u(k+3)-u(k-2)))+20*Bbase(k,k-2,u,u(k+1))/((u(k+4)-u(k))*(u(k+4)-u(k-1)));
A(2,4)=20*Bbase(k-1,k-2,u,u(k+1))/((u(k+3)-u(k-1))*(u(k+4)-u(k-1)))-20*(Bbase(k,k-2,u,u(k+1))/(u(k+4)-u(k)))*(1/(u(k+5)-u(k))+1/(u(k+4)-u(k-1)));
A(2,5)=20*Bbase(k,k-2,u,u(k+1))/((u(k+4)-u(k))*(u(k+5)-u(k)));
%轨迹末加速度系数（边界条件4）
A(n+k-2,n+k-5)=20*Bbase(n+k-3,k-2,u,u(n+k))/((u(n+k+1)-u(n+k-3))*(u(n+k+1)-u(n+k-4)));
A(n+k-2,n+k-4)=20*Bbase(n+k-2,k-2,u,u(n+k))/((u(n+k+2)-u(n+k-2))*(u(n+k+2)-u(n+k-3)))-20*(Bbase(n+k-3,k-2,u,u(n+k))/(u(n+k+1)-u(n+k-3)))*(1/(u(n+k+2)-u(n+k-3))+1/(u(n+k+1)-u(n+k-4)));
A(n+k-2,n+k-3)=20*Bbase(n+k-3,k-2,u,u(n+k))/((u(n+k+1)-u(n+k-3))*(u(n+k+2)-u(n+k-3)))-20*(Bbase(n+k-2,k-2,u,u(n+k))/(u(n+k+2)-u(n+k-2)))*(1/(u(n+k+3)-u(n+k-2))+1/(u(n+k+2)-u(n+k-3)))+20*Bbase(n+k-1,k-2,u,u(n+k))/((u(n+k+3)-u(n+k-1))*(u(n+k+3)-u(n+k-2)));
A(n+k-2,n+k-2)=20*Bbase(n+k-2,k-2,u,u(n+k))/((u(n+k+2)-u(n+k-2))*(u(n+k+3)-u(n+k-2)))-20*(Bbase(n+k-1,k-2,u,u(n+k))/(u(n+k+3)-u(n+k-1)))*(1/(u(n+k+4)-u(n+k-1))+1/(u(n+k+3)-u(n+k-2)));
A(n+k-2,n+k-1)=20*Bbase(n+k-1,k-2,u,u(n+k))/((u(n+k+3)-u(n+k-1))*(u(n+k+4)-u(n+k-1)));

%其他点的系数
for i=3:n+k-3
    for j=0:4
        A(i,i+j-2)=Bbase(i+j-2,k,u,u(3+i));
    end
end

%e:方程右边
e=0;
dt_v_start=20;
dt_v_end=20;
dt_a_start=40;
dt_a_end=40;

e(1,1)=dt_v_start;
e(1,2)=dt_v_start*Y(1,1);

e(n+k-1,1)=dt_v_end;
e(n+k-1,2)=dt_v_end*Y(1,2);

e(2,1)=dt_a_start;
e(2,2)=Y(2,1)*dt_v_start^2+Y(1,1)*dt_a_start;

e(n+k-2,1)=dt_a_end;
e(n+k-2,2)=Y(2,2)*dt_v_end^2+Y(1,2)*dt_a_end;

for i=1:n
    e(i+k-3,:)=X(i,:);
end
%得到控制点,A*d=e
d=inv(A)*e;
%clear A;
%clear e;
%===画出图形================================
hold on
%原始数据,红色,点
plot(X(:,1),X(:,2),'r*');
%控制多边形,蓝色,线
%plot(d(:,1),d(:,2),'k');
%===插值B样条曲线============================
t=0;angle=0;velocity=0;acceleration=0;jerk=0;
down=0;
for j=1:(n-1) %n个vp点对应n段曲线
    for uu=u(j+5):0.005:u(j+6) %第一段曲线从u6到u7 每段内对应有5个控制点 最后一段是u25到u26
        down=down+1;
        x=Bbase(j,5,u,uu)*d(j,:)+Bbase(j+1,5,u,uu)*d(j+1,:)+Bbase(j+2,5,u,uu)*d(j+2,:)+Bbase(j+3,5,u,uu)*d(j+3,:)+Bbase(j+4,5,u,uu)*d(j+4,:)+Bbase(j+5,5,u,uu)*d(j+5,:);
        t(down)=x(1);
        angle(down)=x(2);
        y=Bbase(j+1,4,u,uu)*DeBoorDv(d,u,uu,j+1,1)+Bbase(j+2,4,u,uu)*DeBoorDv(d,u,uu,j+2,1)+Bbase(j+3,4,u,uu)*DeBoorDv(d,u,uu,j+3,1)+Bbase(j+4,4,u,uu)*DeBoorDv(d,u,uu,j+4,1)+Bbase(j+5,4,u,uu)*DeBoorDv(d,u,uu,j+5,1);
        velocity(down)=y(2)/y(1);
        z=Bbase(j+2,3,u,uu)*DeBoorDv(d,u,uu,j+2,2)+Bbase(j+3,3,u,uu)*DeBoorDv(d,u,uu,j+3,2)+Bbase(j+4,3,u,uu)*DeBoorDv(d,u,uu,j+4,2)+Bbase(j+5,3,u,uu)*DeBoorDv(d,u,uu,j+5,2);
        acceleration(down)=(z(2)*y(1)-y(2)*z(1))/y(1)^3;
        f=Bbase(j+3,2,u,uu)*DeBoorDv(d,u,uu,j+3,3)+Bbase(j+4,2,u,uu)*DeBoorDv(d,u,uu,j+4,3)+Bbase(j+5,2,u,uu)*DeBoorDv(d,u,uu,j+5,3);
        jerk(down)=f(2)/y(1)^3-y(2)*f(1)/y(1)^4-3*z(2)*z(1)/y(1)^4+3*y(2)*(z(1)^2)/y(1)^5;
    end 
end
plot(t,angle,'b');
plot(t,velocity,'m');
plot(t,acceleration,'r');
plot(t,jerk,'k');