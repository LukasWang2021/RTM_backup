%************递归求导数控制点,德布尔递推算法************
function y=DeBoorDv(d,u,kn,i,L)
%d控制点序列,u节点序列,kn插入的节点,u(i)<kn<u(i+1),L递推层次
% k B样条次数
k=5;
if L==0
     y=d(i,:);
     return;
end

temp=(u(i+k+1-L)-u(i));
if temp==0 
     y=0;
else
     y=(k+1-L)*(DeBoorDv(d,u,kn,i,L-1)-DeBoorDv(d,u,kn,i-1,L-1))/temp;
end
