%%startup for the SSR prototype model
cd('D:\实时侠同步\RTM_data')
addpath(genpath('SSR_proto'))
%modified dh parameters
qlim1=[-pi/2,pi/2];  
qlim2=[-pi/4,3/4*pi];  
qlim3=[-pi/4,3/4*pi];  
qlim4=[-pi,pi];  
qlim5=[-3/4*pi,3/4*pi];  
qlim6=[-pi,pi];  
% qlim1=[0,pi/2];  
% qlim2=[0,pi/2];  
% qlim3=[0,pi/2];  
% qlim4=[0,pi/2];  
% qlim5=[0,pi/2];  
% qlim6=[0,pi/2];  
Qlim=[qlim1;qlim2;qlim3;qlim4;qlim5;qlim6];
L1 = Link ('d', 0.0645,   'a', 0.084,     'alpha', pi/2, 'offset',0, 'qlim',qlim1,   'r', [-0.024025,-0.092881,-0.012408], 'I', [0.107846,0.056539,0.089210,0.015728,0.000509,-0.000531],  'm', 4.960380,  'G', 81,     'Jm', 1.3/10000);
L2 = Link ('d', 0,       'a', 0.088,  'alpha', 0,    'offset',0, 'qlim',qlim2,'r', [-0.256664,0.004377,-0.029180],  'I', [0.080035,0.973298,0.926635,-0.008252,0.097096,-0.000432], 'm', 10.866126, 'G', 100.908375, 'Jm', 0.59/10000);
L3 = Link ('d', 0,       'a', 0, 'alpha', pi/2, 'offset',pi/2,  'qlim',qlim3,  'r', [-0.011619,-0.019530,0.020880],  'I', [0.048570,0.043418,0.038214,0.004869,-0.000204,0.000130],  'm', 8.462262,  'G', 81.053333,  'Jm', 0.44/10000);
L4 = Link ('d', 0.1765,    'a', 0,     'alpha', -pi/2,'offset',0,  'qlim',qlim4,  'r', [0.000077,0.123799,-0.001418],   'I', [0.088304,0.010729,0.087152,0.000042,-0.000039,-0.001144], 'm', 4.215590,  'G', 59.987882,  'Jm', 0.18/10000);
L5 = Link ('d', 0,       'a', 0,     'alpha', pi/2, 'offset',0,   'qlim',qlim5, 'r', [0.000002,-0.002972,0.022505],   'I', [0.004669,0.004310,0.001795,0,0,0.000007],                 'm', 2.034578,  'G', 66.75495,   'Jm', 0.17/10000); 
L6 = Link ('d', 0.1235,  'a', 0,     'alpha', 0,    'offset',0,   'qlim',qlim6, 'r', [-0.00019,0,-0.004063],          'I', [0.000029,0.000029,0.000052,0,0,0],                        'm', 0.126446,  'G', 44.671266,  'Jm', 0.17/10000);   

robot_sdh = SerialLink([L1,L2,L3,L4,L5,L6], 'name', 'SSR prototype', ...
    'manufacturer', 'Unimation', 'ikine', 'puma', 'comment', 'viscous friction; params of 8/95');
robot_sdh.base = [1 0 0 0;0 1 0 0 ;0 0 1 0;0 0 0 1];

q_give = [0 0 0 0 0 0]
robot_sdh.plot(q_give,'wrist','arrow');
[x,y,z]=workspace_random(robot_sdh,Qlim,100000);%随机取点生成workspace




robot_sdh.teach
