%函数为机器人正解函数，输入6个角度值，得到旋转矩阵

function [T06]=rtm_fkine(robot,u)
L = robot.links;
%   theta                d          a        alpha
DH=[u(1)+ L(1).offset   L(1).d    L(1).a   L(1).alpha;
    u(2)+ L(2).offset   L(2).d    L(2).a   L(2).alpha;   
    u(3)+ L(3).offset   L(3).d    L(3).a   L(3).alpha;
    u(4)+ L(4).offset   L(4).d    L(4).a   L(4).alpha;
    u(5)+ L(5).offset   L(5).d    L(5).a   L(5).alpha;
    u(6)+ L(6).offset   L(6).d    L(6).a   L(6).alpha];


Base =robot.base;
Tbase= [ Base.n Base.o Base.a Base.t;
         0        0        0      1];
Tool= robot.tool;
Ttool=[ Tool.n Tool.o Tool.a Tool.t;
         0       0        0       1];
   
T01=[cos(DH(1,1))   -cos(DH(1,4))*sin(DH(1,1))     sin(DH(1,4))*sin(DH(1,1))   DH(1,3)*cos(DH(1,1));
     sin(DH(1,1))    cos(DH(1,4))*cos(DH(1,1))     -sin(DH(1,4))*cos(DH(1,1))   DH(1,3)*sin(DH(1,1));
     0               sin(DH(1,4))                   cos(DH(1,4))                DH(1,2);
     0               0                             0                            1];     
 
T12=[cos(DH(2,1))   -cos(DH(2,4))*sin(DH(2,1))      sin(DH(2,4))*sin(DH(2,1))   DH(2,3)*cos(DH(2,1));
     sin(DH(2,1))    cos(DH(2,4))*cos(DH(2,1))     -sin(DH(2,4))*cos(DH(2,1))   DH(2,3)*sin(DH(2,1));
     0               sin(DH(2,4))                   cos(DH(2,4))                DH(2,2);
     0               0                             0                            1];
T23=[cos(DH(3,1))   -cos(DH(3,4))*sin(DH(3,1))      sin(DH(3,4))*sin(DH(3,1))   DH(3,3)*cos(DH(3,1));
     sin(DH(3,1))    cos(DH(3,4))*cos(DH(3,1))     -sin(DH(3,4))*cos(DH(3,1))   DH(3,3)*sin(DH(3,1));
     0               sin(DH(3,4))                   cos(DH(3,4))                DH(3,2);
     0               0                             0                            1];
T34=[cos(DH(4,1))   -cos(DH(4,4))*sin(DH(4,1))      sin(DH(4,4))*sin(DH(4,1))   DH(4,3)*cos(DH(4,1));
     sin(DH(4,1))    cos(DH(4,4))*cos(DH(4,1))     -sin(DH(4,4))*cos(DH(4,1))   DH(4,3)*sin(DH(4,1));
     0               sin(DH(4,4))                   cos(DH(4,4))                DH(4,2);
     0               0                             0                            1];
T45=[cos(DH(5,1))   -cos(DH(5,4))*sin(DH(5,1))      sin(DH(5,4))*sin(DH(5,1))   DH(5,3)*cos(DH(5,1));
     sin(DH(5,1))    cos(DH(5,4))*cos(DH(5,1))     -sin(DH(5,4))*cos(DH(5,1))   DH(5,3)*sin(DH(5,1));
     0               sin(DH(5,4))                   cos(DH(5,4))                DH(5,2);
     0               0                             0                            1];
T56=[cos(DH(6,1))   -cos(DH(6,4))*sin(DH(6,1))      sin(DH(6,4))*sin(DH(6,1))   DH(6,3)*cos(DH(6,1));
     sin(DH(6,1))    cos(DH(6,4))*cos(DH(6,1))     -sin(DH(6,4))*cos(DH(6,1))   DH(6,3)*sin(DH(6,1));
     0               sin(DH(6,4))                   cos(DH(6,4))                DH(6,2);
     0               0                             0                            1];

T06=Tbase*T01*T12*T23*T34*T45*T56*Ttool;
end



