function [ikine]=rtm_ikine(robot,Ttotal,arm,elbow,wrist,flip)

%首先处理各个姿态宏的默认0
    if arm==0
        arm=1;
    end
    if elbow==0
        elbow=1; 
    end
    if wrist==0
        wrist=1;
    end
    if flip~=0
        flip=1;
    end

%引入机器人参数
L = robot.links;
    
   
%关节偏移角
offset2= robot.offset(2);


Base =robot.base;  
Tbase= [ Base.n Base.o Base.a Base.t;
         0        0        0      1];
Tool= robot.tool;
Ttool=[ Tool.n Tool.o Tool.a Tool.t;
         0       0        0       1];
     
Tbe = Tbase\Ttotal*Ttool.';


%将计算得到的1-6的旋转矩阵参数传入；
 ne = Tbe(1:3,1);
 se = Tbe(1:3,2);
 ae = Tbe(1:3,3);

%简化一些后文常用的参数的写法，方便写代码
d1=L(1).d;d2=L(2).d;d3=L(3).d;d4=L(4).d;d5=L(5).d;d6=L(6).d;
a1=L(1).a;a2=L(2).a;a3=L(3).a;a4=L(4).a;a5=L(5).a;a6=L(6).a;
f1=L(1).alpha;f2=L(2).alpha;f3=L(3).alpha;f4=L(4).alpha;f5=L(5).alpha;f6=L(6).alpha;
%  a1=MDH(1,3);a2=MDH(2,3);a3=MDH(3,3);d2=0;d3=0;f1=-pi/2;f3=-pi/2;f4=pi/2;f5=-pi/2;
 %t1(3,4)
 
 %处理pe，将法兰中心坐标转到腕关节中心
  
 pe = Tbe(1:3,4)-d6*ae;
 px = pe(1);py = pe(2);pz=pe(3);
 
 
 
R=sqrt(px^2+py^2+pz^2);
r=sqrt(px^2+py^2);

%q1
q1=atan2(arm*py,arm*px);

  if q1<-pi
        q1 = q1+2*pi;
  end
  if q1>pi
        q1 = q1-2*pi;
  end
  if abs(q1)<1e-6
    q1=0;
  end  
  
%q2
px1=px-a1*cos(q1);
py1=py-a1*sin(q1);
pz1=pz-d1;

R1=sqrt(px1^2+py1^2+pz1^2);
r1=sqrt(px1^2+py1^2);

sin_alpha=pz1/R1;
cos_alpha=arm*r1/R1;

alpha=atan2(sin_alpha,cos_alpha);

cos_beta=(a2^2+R1^2-(d4^2+a3^2))/(2*a2*R1);
sin_beta=sqrt(1-cos_beta^2);
beta= atan2(sin_beta,cos_beta);

q2 = alpha+elbow*beta;

 if q2<-pi
        q2 = q2+2*pi;
 end
 if q2>pi
        q2 = q2-2*pi;
 end
 q2=q2-offset2;
  if abs(q2)<1e-6
    q2=0;
  end  
%q3
    
cos_phi = (a2^2+d4^2+a3^2-R1^2)/(2*a2*sqrt(d4^2+a3^2));
sin_phi = -elbow*sqrt(1-cos_phi^2);

phi = atan2(sin_phi,cos_phi);

sin_beta = d4/sqrt(d4^2+a3^2);
cos_beta = abs(a3)/sqrt(d4^2+a3^2);
beta = atan2(sin_beta,cos_beta);

sin_q3 = -sin(beta-phi);
cos_q3 = -cos(beta-phi);
q3 = atan2(sin_q3,cos_q3);

if q3<-pi
        q3 = q3+2*pi;
end
if q3>pi
        q3 = q3-2*pi;
end
 if abs(q3)<1e-6
    q3=0;
  end  
% 
% cos_phi=(a2^2+d4^2+a3^2-R1^2)/(2*a2*sqrt(d4^2+a3^2));
% sin_phi=arm*elbow*sqrt(1-cos_phi^2);
% sin_beta4=d4/sqrt(d4^2+a3^2);
% cos_beta4=abs(a3)/sqrt(d4^2+a3^2);
% sin_q3=sin_phi*cos_beta4-cos_phi*sin_beta4;
% cos_q3=cos_phi*cos_beta4+sin_phi*sin_beta4;
% q3=atan2(sin_q3,cos_q3);
%  if q3<-pi
%         q3 = q3+2*pi;
%  end
%  if q3>pi
%         q3 = q3-2*pi;
%  end
%     
    
    %q4
%   theta           d          a        alpha
DH=[L(1).offset+q1   L(1).d    L(1).a   L(1).alpha;
    L(2).offset+q2   L(2).d    L(2).a   L(2).alpha;   
    L(3).offset+q3   L(3).d    L(3).a   L(3).alpha];

T01=[cos(DH(1,1))   -cos(DH(1,4))*sin(DH(1,1))     sin(DH(1,4))*sin(DH(1,1))   DH(1,3)*cos(DH(1,1));
     sin(DH(1,1))    cos(DH(1,4))*cos(DH(1,1))     -sin(DH(1,4))*cos(DH(1,1))   DH(1,3)*sin(DH(1,1));
     0               sin(DH(1,4))                   cos(DH(1,4))                DH(1,2);
     0               0                             0                            1];     
 
T12=[cos(DH(2,1))   -cos(DH(2,4))*sin(DH(2,1))      sin(DH(2,4))*sin(DH(2,1))   DH(2,3)*cos(DH(2,1));
     sin(DH(2,1))    cos(DH(2,4))*cos(DH(2,1))     -sin(DH(2,4))*cos(DH(2,1))   DH(2,3)*sin(DH(2,1));
     0               sin(DH(2,4))                   cos(DH(2,4))                DH(2,2);
     0               0                             0                            1];
T23=[cos(DH(3,1))   -cos(DH(3,4))*sin(DH(3,1))      sin(DH(3,4))*sin(DH(3,1))   DH(3,3)*cos(DH(3,1));
     sin(DH(3,1))    cos(DH(3,4))*cos(DH(3,1))     -sin(DH(3,4))*cos(DH(3,1))   DH(3,3)*sin(DH(3,1));
     0               sin(DH(3,4))                   cos(DH(3,4))                DH(3,2);
     0               0                             0                            1];
T03=T01*T12*T23;

 x3 = T03(1:3,1);
 y3 = T03(1:3,2);
 z3 = T03(1:3,3);
 
z3ae = cross(z3,ae);
    sign_ang5 = sign(z3ae(2));
    if sign_ang5==0
        sign_ang5=1;
    end
z4_d_x3 = dot(z3ae,x3);
z4_d_y3 = dot(z3ae,y3);


     if sign_ang5==1
        if wrist==1
            %z3ae向量 与z4反向 z3 旋转大角度
            sq4 = z4_d_x3;
            cq4 = -z4_d_y3;

            q4 = atan2(sq4,cq4);
            
        else
            %z3ae向量 与z4同向 z3 旋转小角度
            sq4 = -z4_d_x3;
            cq4 = z4_d_y3;

            q4 = atan2(sq4,cq4);
        end
    else
        if wrist==1
            sq4 = -z4_d_x3;
            cq4 = z4_d_y3;

            q4 = atan2(sq4,cq4);

        else
            sq4 = z4_d_x3;
            cq4 = -z4_d_y3;

            q4 = atan2(sq4,cq4);

        end
    end


    if q4<-pi
        q4 = q4+2*pi;
    end
    if q4>pi
        q4 = q4-2*pi;
    end


%     if flip==1

%         q4 = q4+pi;
%     end
    
%     if q4<-pi
%         q4 = q4+2*pi;
%     end
%     if q4>pi
%         q4 = q4-2*pi;
%     end
 if abs(q4)<1e-6
    q4=0;
  end  
%   theta           d          a        alpha
% DH=[L(1).offset   L(1).d    L(1).a   L(1).alpha;
%     L(2).offset   L(2).d    L(2).a   L(2).alpha;   
%     L(3).offset   L(3).d    L(3).a   L(3).alpha];
% 
% T01=[cos(DH(1,1))   -cos(DH(1,4))*sin(DH(1,1))     sin(DH(1,4))*sin(DH(1,1))   DH(1,3)*cos(DH(1,1));
%      sin(DH(1,1))    cos(DH(1,4))*cos(DH(1,1))     -sin(DH(1,4))*cos(DH(1,1))   DH(1,3)*sin(DH(1,1));
%      0               sin(DH(1,4))                   cos(DH(1,4))                DH(1,2);
%      0               0                             0                            1];     
%  
% T12=[cos(DH(2,1))   -cos(DH(2,4))*sin(DH(2,1))      sin(DH(2,4))*sin(DH(2,1))   DH(2,3)*cos(DH(2,1));
%      sin(DH(2,1))    cos(DH(2,4))*cos(DH(2,1))     -sin(DH(2,4))*cos(DH(2,1))   DH(2,3)*sin(DH(2,1));
%      0               sin(DH(2,4))                   cos(DH(2,4))                DH(2,2);
%      0               0                             0                            1];
% T23=[cos(DH(3,1))   -cos(DH(3,4))*sin(DH(3,1))      sin(DH(3,4))*sin(DH(3,1))   DH(3,3)*cos(DH(3,1));
%      sin(DH(3,1))    cos(DH(3,4))*cos(DH(3,1))     -sin(DH(3,4))*cos(DH(3,1))   DH(3,3)*sin(DH(3,1));
%      0               sin(DH(3,4))                   cos(DH(3,4))                DH(3,2);
%      0               0                             0                            1];
% T03=T01*T12*T23;
% 
%  x3 = T03(1:3,1);
%  y3 = T03(1:3,2);
%  z3 = T03(1:3,3);
% 
% y5=cross(z3,ae);
% z4=y5;
% M=wrist*sign(se.*y5);
% sin_q4=M.*(z4.*x3);
% cos_q4=M.*(z4.*y3);
% q4=atan2(sin_q4,cos_q4);
%   if q4<-pi
%         q4 = q4+2*pi;
%   end
%   if q4>pi
%         q4 = q4-2*pi;
%   end

%q5
DH=[L(1).offset+q1   L(1).d    L(1).a   L(1).alpha;
    L(2).offset+q2   L(2).d    L(2).a   L(2).alpha;   
    L(3).offset+q3   L(3).d    L(3).a   L(3).alpha;
    L(4).offset+q4   L(4).d    L(4).a   L(4).alpha;];
T34=[cos(DH(4,1))   -cos(DH(4,4))*sin(DH(4,1))      sin(DH(4,4))*sin(DH(4,1))   DH(4,3)*cos(DH(4,1));
     sin(DH(4,1))    cos(DH(4,4))*cos(DH(4,1))     -sin(DH(4,4))*cos(DH(4,1))   DH(4,3)*sin(DH(4,1));
     0               sin(DH(4,4))                   cos(DH(4,4))                DH(4,2);
     0               0                             0                            1];
T04=T03*T34;
 x4 = T04(1:3,1);
 y4 = T04(1:3,2);
 z4 = T04(1:3,3);
 
sin_q5 = dot(ae,x4) ;
if abs(sin_q5)<1e-6
    sin_q5=0;
end
cos_q5 = -dot(ae,y4);
if abs(cos_q5)<1e-6
    cos_q5=0;
end

q5 = atan2(sin_q5,cos_q5);

if q5<-pi
    q5 = q5+2*pi;
end
if q5>pi
    q5 = q5-2*pi;
end

% if flip==1
%     q5 = -q5;
% end
 if abs(q5)<1e-6
    q5=0;
  end  

%q6
y5=z4;

sin_q6=dot(ne,y5);
if abs(sin_q6)<1e-6
    sin_q6=0;
end

cos_q6= dot(se,y5);
if abs(cos_q6)<1e-6
    cos_q6=0;
end  

q6=atan2(sin_q6,cos_q6);

 if q6<-pi
    q6 = q6+2*pi;
 end
 if q6>pi
    q6 = q6-2*pi;
 end
 if abs(q6)<1e-6
    q6=0;
 end
%  if flip==1
%         q6 = q6+pi;
%  end
 if q6<-pi
    q6 = q6+2*pi;
 end
 if q6>pi
    q6 = q6-2*pi;
 end
    solve(1) = q1;
    solve(2) = q2;
    solve(3) = q3;
    solve(4) = q4;
    solve(5) = q5;
    solve(6) = q6;

    j_max = robot.qlim(:,2);
    j_min = robot.qlim(:,1);
  
    for i=1:6
        if solve(i)<j_min(i) || solve(i)>j_max(i)
            error('ikine: joint %d is out of range',i)      
        end
    end        
ikine=[q1 q2 q3 q4 q5 q6];
end



%  t11=-atan2(-py,px)+atan2((d2-d3)/sin(f1),((px*sin(f1))^2+(py*sin(f1))^2-(d2-d3)^2)^0.5);
%  t12=-atan2(-py,px)+atan2((d2-d3)/sin(f1),-((px*sin(f1))^2+(py*sin(f1))^2-(d2-d3)^2)^0.5);
%  %t3
%  m3_1=pz*sin(f1);
%  n3_1=a1-px*cos(t11)-py*sin(t11);
%  m3_2=pz*sin(f1);
%  n3_2=a1-px*cos(t12)-py*sin(t12);
%  t31=-atan2(a2*a3/sin(f3),a2*d4)+atan2((m3_1^2+n3_1^2-a2^2-a3^2-d4^2)/sin(f3),((2*a2*d4*sin(f3))^2+(2*a2*a3)^2-(m3_1^2+n3_1^2-a2^2-a3^2-d4^2)^2)^0.5);
%  t32=-atan2(a2*a3/sin(f3),a2*d4)+atan2((m3_1^2+n3_1^2-a2^2-a3^2-d4^2)/sin(f3),-((2*a2*d4*sin(f3))^2+(2*a2*a3)^2-(m3_1^2+n3_1^2-a2^2-a3^2-d4^2)^2)^0.5);
%  t33=-atan2(a2*a3/sin(f3),a2*d4)+atan2((m3_2^2+n3_2^2-a2^2-a3^2-d4^2)/sin(f3),((2*a2*d4*sin(f3))^2+(2*a2*a3)^2-(m3_2^2+n3_2^2-a2^2-a3^2-d4^2)^2)^0.5);
%  t34=-atan2(a2*a3/sin(f3),a2*d4)+atan2((m3_2^2+n3_2^2-a2^2-a3^2-d4^2)/sin(f3),-((2*a2*d4*sin(f3))^2+(2*a2*a3)^2-(m3_2^2+n3_2^2-a2^2-a3^2-d4^2)^2)^0.5);
%  %t2
%  m2_1=a2+a3*cos(t31)+d4*sin(f3)*sin(t31);
%  n2_1=a3*sin(t31)-d4*sin(f3)*cos(t31);
%  m2_2=a2+a3*cos(t32)+d4*sin(f3)*sin(t32);
%  n2_2=a3*sin(t32)-d4*sin(f3)*cos(t32);
%  m2_3=a2+a3*cos(t33)+d4*sin(f3)*sin(t33);
%  n2_3=a3*sin(t33)-d4*sin(f3)*cos(t33);
%  m2_4=a2+a3*cos(t34)+d4*sin(f3)*sin(t34);
%  n2_4=a3*sin(t34)-d4*sin(f3)*cos(t34);
%  t21=atan2(m3_1*m2_1+n2_1*n3_1,m3_1*n2_1-m2_1*n3_1);
%  t22=atan2(m3_1*m2_2+n2_2*n3_1,m3_1*n2_2-m2_2*n3_1);
%  t23=atan2(m3_2*m2_3+n2_3*n3_2,m3_2*n2_3-m2_3*n3_2);
%  t24=atan2(m3_2*m2_4+n2_4*n3_2,m3_2*n2_4-m2_4*n3_2);
%  %t5
%  m5_1=-sin(f5)*(ax*cos(t11)*cos(t21)+ay*sin(t11)*cos(t21)+az*sin(f1)*sin(t21));
%  n5_1=sin(f5)*(ax*cos(t11)*sin(t21)+ay*sin(t11)*sin(t21)-az*sin(f1)*cos(t21));
%  m5_2=-sin(f5)*(ax*cos(t11)*cos(t22)+ay*sin(t11)*cos(t22)+az*sin(f1)*sin(t22));
%  n5_2=sin(f5)*(ax*cos(t11)*sin(t22)+ay*sin(t11)*sin(t22)-az*sin(f1)*cos(t22));
%  m5_3=-sin(f5)*(ax*cos(t12)*cos(t23)+ay*sin(t12)*cos(t23)+az*sin(f1)*sin(t23));
%  n5_3=sin(f5)*(ax*cos(t12)*sin(t23)+ay*sin(t12)*sin(t23)-az*sin(f1)*cos(t23));
%  m5_4=-sin(f5)*(ax*cos(t12)*cos(t24)+ay*sin(t12)*cos(t24)+az*sin(f1)*sin(t24));
%  n5_4=sin(f5)*(ax*cos(t12)*sin(t24)+ay*sin(t12)*sin(t24)-az*sin(f1)*cos(t24));
% 
%  t51=atan2(((ay*cos(t11)-ax*sin(t11))^2+(m5_1*cos(t31)+n5_1*sin(t31))^2)^0.5,(m5_1*sin(t31)-n5_1*cos(t31))/(sin(f3)*sin(f4)));
%  t52=atan2(-((ay*cos(t11)-ax*sin(t11))^2+(m5_1*cos(t31)+n5_1*sin(t31))^2)^0.5,(m5_1*sin(t31)-n5_1*cos(t31))/(sin(f3)*sin(f4)));
%  t53=atan2(((ay*cos(t11)-ax*sin(t11))^2+(m5_2*cos(t32)+n5_2*sin(t32))^2)^0.5,(m5_2*sin(t32)-n5_2*cos(t32))/(sin(f3)*sin(f4)));
%  t54=atan2(-((ay*cos(t11)-ax*sin(t11))^2+(m5_2*cos(t32)+n5_2*sin(t32))^2)^0.5,(m5_2*sin(t32)-n5_2*cos(t32))/(sin(f3)*sin(f4)));
% 
%  t55=atan2(((ay*cos(t12)-ax*sin(t12))^2+(m5_3*cos(t33)+n5_3*sin(t33))^2)^0.5,(m5_3*sin(t33)-n5_3*cos(t33))/(sin(f3)*sin(f4)));
%  t56=atan2(-((ay*cos(t12)-ax*sin(t12))^2+(m5_3*cos(t33)+n5_3*sin(t33))^2)^0.5,(m5_3*sin(t33)-n5_3*cos(t33))/(sin(f3)*sin(f4)));
%  t57=atan2(((ay*cos(t12)-ax*sin(t12))^2+(m5_4*cos(t34)+n5_4*sin(t34))^2)^0.5,(m5_4*sin(t34)-n5_4*cos(t34))/(sin(f3)*sin(f4)));
%  t58=atan2(-((ay*cos(t12)-ax*sin(t12))^2+(m5_4*cos(t34)+n5_4*sin(t34))^2)^0.5,(m5_4*sin(t34)-n5_4*cos(t34))/(sin(f3)*sin(f4)));
% 
%  %t4
%  if sin(t51)==0
%      t41=0;
%  else 
%      t41=atan2(((ay*cos(t11)-ax*sin(t11))*sin(f1)*sin(f5))/(-sin(t51)*sin(f3)),(-m5_1*cos(t31)-n5_1*sin(t31))/(sin(t51)));
%  end
%  if sin(t52)==0 
%      t42=0;
%  else 
%      t42=atan2(((ay*cos(t11)-ax*sin(t11))*sin(f1)*sin(f5))/(-sin(t52)*sin(f3)),(-m5_1*cos(t31)-n5_1*sin(t31))/(sin(t52)));
%  end
%  if sin(t53)==0 
%      t43=0;
%  else 
%      t43=atan2(((ay*cos(t11)-ax*sin(t11))*sin(f1)*sin(f5))/(-sin(t53)*sin(f3)),(-m5_2*cos(t32)-n5_2*sin(t32))/(sin(t53)));
%  end
%  if sin(t54)==0 
%      t44=0;
%  else 
%      t44=atan2(((ay*cos(t11)-ax*sin(t11))*sin(f1)*sin(f5))/(-sin(t54)*sin(f3)),(-m5_2*cos(t32)-n5_2*sin(t32))/(sin(t54)));
%  end
% 
%  if sin(t55)==0
%      t45=0;
%  else 
%      t45=atan2(((ay*cos(t12)-ax*sin(t12))*sin(f1)*sin(f5))/(-sin(t55)*sin(f3)),(-m5_3*cos(t33)-n5_3*sin(t33))/(sin(t55)));
%  end
%  if sin(t56)==0 
%      t46=0;
%  else 
%      t46=atan2(((ay*cos(t12)-ax*sin(t12))*sin(f1)*sin(f5))/(-sin(t56)*sin(f3)),(-m5_3*cos(t33)-n5_3*sin(t33))/(sin(t56)));
%  end
%  if sin(t57)==0 
%      t47=0;
%  else 
%      t47=atan2(((ay*cos(t12)-ax*sin(t12))*sin(f1)*sin(f5))/(-sin(t57)*sin(f3)),(-m5_4*cos(t34)-n5_4*sin(t34))/(sin(t57)));
%  end
%  if sin(t58)==0 
%      t48=0;
%  else 
%      t48=atan2(((ay*cos(t12)-ax*sin(t12))*sin(f1)*sin(f5))/(-sin(t58)*sin(f3)),(-m5_4*cos(t34)-n5_4*sin(t34))/(sin(t58)));
%  end
%  %t6
%  e1=nx*sin(t11)-ny*cos(t11);
%  f1=ox*sin(t11)-oy*cos(t11);
%  t61=atan2((cos(t41)*e1-cos(t51)*sin(t41)*f1),(cos(t41)*f1+cos(t51)*sin(t41)*e1));
%  t62=atan2((cos(t42)*e1-cos(t52)*sin(t42)*f1),(cos(t42)*f1+cos(t52)*sin(t42)*e1));
%  t63=atan2((cos(t43)*e1-cos(t53)*sin(t43)*f1),(cos(t43)*f1+cos(t53)*sin(t43)*e1));
%  t64=atan2((cos(t44)*e1-cos(t54)*sin(t44)*f1),(cos(t44)*f1+cos(t54)*sin(t44)*e1));
% 
%  e2=nx*sin(t12)-ny*cos(t12);
%  f2=ox*sin(t12)-oy*cos(t12);
%  t65=atan2((cos(t45)*e2-cos(t55)*sin(t45)*f2),(cos(t45)*f2+cos(t55)*sin(t45)*e2));
%  t66=atan2((cos(t46)*e2-cos(t56)*sin(t46)*f2),(cos(t46)*f2+cos(t56)*sin(t46)*e2));
%  t67=atan2((cos(t47)*e2-cos(t57)*sin(t47)*f2),(cos(t47)*f2+cos(t57)*sin(t47)*e2));
%  t68=atan2((cos(t48)*e2-cos(t58)*sin(t48)*f2),(cos(t48)*f2+cos(t58)*sin(t48)*e2));
% 
%  ikine=[t11 t21 t31 t41 t51 t61;
%           t11 t21 t31 t42 t52 t62;
%           t11 t22 t32 t43 t53 t63;
%           t11 t22 t32 t44 t54 t64
%           t12 t23 t33 t45 t55 t65;
%           t12 t23 t33 t46 t56 t66;
%           t12 t24 t34 t47 t57 t67;
%           t12 t24 t34 t48 t58 t68];