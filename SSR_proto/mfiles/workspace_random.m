function [x,y,z]=workspace_random(robot,Q,N)
% k=0.1;
% i=1;
% P=[];
% % for q0=0:k:pi/2
%     for q1=0:k:pi/2
%         for q2=0:k:pi/2
%             for q3=0:k:pi/2
%                 for q4=0:k:pi/2
%                     for q5=0:k:pi/2
%                     T06 = rtm_fkine(robot_sdh,[0 q1 q2 q3 q4 q5]);     
%                     P(i,:)= T06(1:3,4)';
%                     i=i+1;
%                     end
%                 end
%             end
%         end
%     end
% % end
% x=P(:,1);
% y=P(:,2);
% z=P(:,3);
% scatter3(x,y,z,50,z,'.');
% xlabel('x')
% ylabel('y')

% N = 10000;                         %点的个数
% Q=[qlim1;qlim2;qlim3;qlim4;qlim5;qlim6];
for i=1:N
    q = Q(:,1) + (Q(:,2)-Q(:,1)).*rand(6,1); %随机生成6个关节角
    T06 = rtm_fkine(robot, q);               %获得T阵
    x(i) = T06(1,4);
    y(i) = T06(2,4);
    z(i) = T06(3,4);
end

wkspace=alphaShape(x,y,z);
[tri, xyz] = boundaryFacets(wkspace);
trisurf(tri,xyz(:,1),xyz(:,2),xyz(:,3),...
xyz(:,3),'FaceAlpha',0.6)


x=x';
y=y';
z=z';
scatter3(x,y,z,50,z','.');
% F = scatteredInterpolant(x,y,z);
% Density = 100;
% [xq,yq] = meshgrid(linspace(min(x), max(x), Density), linspace(min(y), max(y), Density));
% F.Method = 'linear';
% znew = F(xq,yq);
plot3(x,y,z,'mo')

end
% [X,Y]=meshgrid(x,y);
% Z=diag(z);
% mesh(X,Y,Z)