%%trajectory planning for two joints  J=traj_planning(time_interval,VP,tn,kT,kJ,plot_flag)

% VP=[0.1 0.2 0.3 0.4 0.5 0.6;0.6 0.5 0.4 0.3 0.2 0.1]; %given via points
% k=1;
% vel_ub=[1*k;2*k];%vel upper and lower bound
% vel_lb=[-1*k;-2*k];
% acc_ub=[10*k;20*k];
% acc_lb=[-10*k;-20*k];
% jerk_ub=[100*k;200*k];
% jerk_lb=[-100*k;-200*k];

function [J,y,VP,CP,Lambda]=traj_planning(time_interval,VP,tn,kT,kJ,plot_flag)
%% one joint set up
% h=time_interval./sum(time_interval);  %是否映射到01区间
h=time_interval;
VP=[zeros(length(VP),1) VP'];% VP的时间间隔预设为0
for n=1:length(VP)
   if n==1
       VP(n,:)=[0 VP(1,2)]; % 时间从0开始 每次递增hi
   else
       VP(n,:)=[VP(n-1,1)+h(n-1) VP(n,2)];
   end
end   
vs=0;
as=0;
js=0;
ve=0;
ae=0;
je=0;
[Nvp,Nj]=size(VP);%number of joints and via points
p=5;%order k = p+1 p is degree
n=Nvp+p+1; %Number of control points.=vp + p -1 for normal. Here we need js=je=0 so need two more control points. n=conditions(vs as js ve ae je=6) + VP
m=n+p+1;%Number of knots
% del=1/(m-2*p-1);%中间knots均匀分布
% knots=[zeros(1,p+1) 0+del:del:1-del ones(1,p+1)]; 
% A=[VP(1,1) 0 0 0 VP(2:end,1)' 1 1 1]; %规定限制条件是在初始和末端位置处的速度加速度 0 0 0代表初始位置 速度 加速度等式 1 1 1代表末端 类似Ax=b
% b=[VP(1,2) vs as js VP(2:end,2)' ve ae je];
del=sum(h)/(m-2*p-1);
knots=[zeros(1,p+1) 0+del:del:sum(h)-del sum(h)*ones(1,p+1)]; %均匀节点
A=[VP(1,1) 0 0 0 VP(2:end,1)' VP(end,1) VP(end,1) VP(end,1)]; %规定限制条件是在初始和末端位置处的速度加速度 0 0 0代表初始位置 速度 加速度等式 1 1 1代表末端 类似Ax=b
b=[VP(1,2) vs as js VP(2:end,2)' ve ae je];
f=spapi(knots,A,b);%根据A*Qi=b的限定等式求解控制点Qi
Q=f.coefs;%need vp+p-1 control points
ti=knots; %clamped knots vector
j=1;
y=[];
% for t = tn
%     Bp=0;
%     for i=1:n
%         N=basefcn(t,ti,i,p);
%         Bp=Bp+Q(i).*N;
% %         Bp=Bp+Q(i,:).*N;
%         M(j,i)=N;
%     end
% %     y(j,:)=Bp;
%     y(j)=Bp;
%     j=j+1;
% end
interval=sum(time_interval)/1000;
tn=[0:interval:sum(time_interval)];%离散采样最终时长应该与当前的time interval和相同
y=fnval(f,tn);
df = fnder(f,1); ddf = fnder(f,2);dddf = fnder(f,3);  % derivatives 
vel = fnval(df,tn);
acc = fnval(ddf,tn);
jerk = fnval(dddf,tn);
Lambda=[max(abs(df.coefs)) max(abs(ddf.coefs)) max(abs(dddf.coefs))];
if plot_flag==1
plot(VP(:,1),VP(:,2),'r*');hold on
plot(tn,y);hold on
plot(tn,vel,tn,acc,tn,jerk)
% plot(VP(:,1),VP(:,2));
plot(VP(:,1)',Q(p-1:end-p+2),'blue');
end
CP=Q(p-1:end-p+2);
J=kT*(sum(time_interval)*ones(1,length(tn)))+kJ*abs(jerk);
% J=jerk;
end
%% knots interval optimization


%% generate Bp
% f=spapi(knots,A,b);
% n=length(VP)+p-1;%control points = 9
% m=p+n+1;%nodes
% tn=[0:0.01:1];%time
% % Q=[VP(1,:);2 2;3 4;VP(2,:);VP(3,:);7 4;8 3;VP(end,:)];%control points
% Q=f.coefs;%need vp+p-1 control points
% ti=[0 0 0 0 0 0 0.5 0.5 0.5 1 1 1 1 1 1]; %clamped knots vector
% j=1;
% for t = tn
%     Bp=0;
%     for i=1:n
%         N=basefcn(t,ti,i,p);
%         Bp=Bp+Q(i).*N;
% %         Bp=Bp+Q(i,:).*N;
%         M(j,i)=N;
%     end
% %     y(j,:)=Bp;
%     y(j)=Bp;
%     j=j+1;
% end
% 
% % plot(y(:,1),y(:,2));hold on
% % plot(Q(:,1),Q(:,2),'*r')
% figure
% plot(tn,y);hold on
% plot(VP(:,1),VP(:,2),'r*');

%% 5 order base function, t is current time ti is knot time vector,i is current segment
function N=basefcn(t,ti,i,p)
    if p==0
        if ti(i)<=t && t<ti(i+1)
            N=1;
        elseif t==ti(end) && t==ti(i+1)
            N=1;
        else
            N=0;
        end
    else
        N_i_p_1=basefcn(t,ti,i,p-1);
        N_i1_p_1=basefcn(t,ti,i+1,p-1);
        L1=ti(i+p)-ti(i);
        L2=ti(i+p+1)-ti(i+1);
        if L1==0
            L1=1;
        end
        if L2==0
            L2=1;
        end      
        N_i_p=(t-ti(i))/L1*N_i_p_1+(ti(i+p+1)-t)/L2*N_i1_p_1;
        N=N_i_p;
    end
end


