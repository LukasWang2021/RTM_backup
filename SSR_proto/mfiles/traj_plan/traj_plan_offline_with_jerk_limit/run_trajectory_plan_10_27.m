kT=100;% Time cost function gain
kJ=1e-6;% jerk cost function gain
VC=200;% rad/s limitatioin 
AC=5000;
JC=20000;
t=[0:0.1:3];
VPx=sin(0.5*pi*t);
VPy=cos(0.5*pi*t);
% VPx=[1 2 3 4 5 6 7]; %via points x axis
% VPy=[1 3 5 3 2 0 3]; %via points y axis
for n=1:length(VPx)-1
    vc(n)=VPx(n+1)-VPx(n);
end
for n=1:length(vc)-1
    ac(n)=vc(n+1)-vc(n);
end
for n=1:length(ac)-1
    jc(n)=ac(n+1)-ac(n);
end
hv=2*max(abs(vc))/VC;
ha=2*max(abs(ac))/AC;
hj=2*max(abs(jc))/JC;
% ha=2*(2*max(abs(vc))/AC)^1/2;
% hj=2*(6*max(abs(vc))/JC)^1/3;
h_min=max([hv ha hj]); % find minimum interval h_min
h_init=1*h_min*ones(1,length(VPx)-1); %VP-1个time_interval hi
tn=[0:0.001:sum(h_init)];
h_init=1*h_min*ones(1,length(VPx)-1); %VP-1个time_interval hi
J=traj_planning(h_init,VPx,tn,kT,kJ,0);
optimum_func = @(parameters)costfuncxy(parameters,VPx,VPy,tn,kT,kJ);%parameters=time_interval
options = optimoptions(@lsqnonlin,'MaxIterations',5000,'MaxFunctionEvaluations',500); %'Algorithm','levenberg-marquardt'
% [~,x,~,CPx,Lambdax]=traj_planning(h_init,VPx,tn,kT,kJ,0);
% [~,y,~,CPy,Lambday]=traj_planning(h_init,VPy,tn,kT,kJ,0);
% Lambda=max([Lambdax;Lambday]);%计算h_init 条件下p v a的系数最大值
% Lambda=Lambda./[VC AC JC];
% Lambda=max([1 Lambda(1) Lambda(2)^(1/2) Lambda(3)^(1/3)]);
% h_init=h_init*Lambda;
%%
lb=h_init;
ub=20*lb;
h_optimum = lsqnonlin(optimum_func,h_init,lb,ub,options);

tn=[0:0.01:sum(h_init)];
figure
traj_planning(h_init,VPx,tn,kT,kJ,1);
xlabel('time s')
legend('Via points','Pos','Vel','Acc','Jerk')
title('before optimization')
% 
tn=[0:0.01:sum(h_optimum)];
figure
subplot(2,1,1)
traj_planning(h_optimum,VPx,tn,kT,kJ,1);
subplot(2,1,2)
traj_planning(h_optimum,VPy,tn,kT,kJ,1);
xlabel('time s')
legend('Via points','Pos','Vel','Acc','Jerk')
title('after optimization')

[J1,x,VPx,CPx]=traj_planning(h_optimum,VPx,tn,kT,kJ,0);
[J2,y,VPy,CPy]=traj_planning(h_optimum,VPy,tn,kT,kJ,0);

figure 
hold on
plot(x,y)
plot(VPx(:,2),VPy(:,2),'r*')
plot(CPx,CPy,'bo')
figure
subplot(2,1,1)
hold on
plot(tn,x);
plot(VPx(:,1),CPx','bo',VPx(:,1),VPx(:,2),'r*');
subplot(2,1,2)
hold on
plot(tn,y);
plot(VPy(:,1),CPy','bo',VPy(:,1),VPy(:,2),'r*');

function J=costfuncxy(time_interval,VPx,VPy,tn,kT,kJ)
[J1,x,~,~]=traj_planning(time_interval,VPx,tn,kT,kJ,0);
[J2,y,~,~]=traj_planning(time_interval,VPy,tn,kT,kJ,0);
J2=0;
% for i = 1:length(x)-1
%     k(i)=norm([x(i+1)-x(i),y(i+1)-y(i)]);
% end
J=mean(abs(J1)+abs(J2))
end


