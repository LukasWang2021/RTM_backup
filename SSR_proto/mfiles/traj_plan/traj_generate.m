%%[Ponline,Qonline,T]=traj_generate(textname)用于读取touch生成的T矩阵序列，进行坐标转换和轨迹绘制。
function [Ponline,Qonline,T]=traj_generate(textname)
addpath(genpath('D:\实时侠同步\RTM_data\SSR_proto\traj_online_input'))
try
robot_sdh=evalin('base','robot_sdh');
catch
addpath(genpath('D:\实时侠同步\RTM_data\rtm_mfiles'))
run('rtm_robot_sdh.m')
end
input_traj=readmatrix(textname,'Delimiter',':');
ts=0.1;%采样周期
Rrv=[0 0 -1;
     -1 0 0;
     0  1 0];
Rrv2=[0 0 -1;
     1 0 0;
     0 -1 0];
N=length(input_traj);
RPY=zeros(N,6);
theta=zeros(N,1); %rotation to angel-vector form
v=zeros(N,3); %vector
T={};
for k=1:length(input_traj)
Rin=[input_traj(k,1:3)' input_traj(k,5:7)' input_traj(k,9:11)'];
Rr=Rrv2*Rin;
Pin(k,:)=input_traj(k,13:15);
Pr(k,:)=0.2*(Rrv*Pin(k,:)')'+[0.36,0,0.45]; %0.2是比例系数
T{k}=[Rr(1,:) Pr(k,1);
      Rr(2,:) Pr(k,2);
      Rr(3,:) Pr(k,3);
      0 0 0 1];
RPY(k,:)=[Pr(k,1) Pr(k,2) Pr(k,3) tr2rpy(T{k},'xyz')];
Quat(k,:)=real(rtm_r2angvec(Rr));
% Quat(k,:)=rotm2quat(Rr);
% q(k,:)=rtm_ikine(robot_sdh,T{k},1,1,1,0);
% plot3(1*Pin(k,1),Pin(k,2),Pin(k,3),'x')
% hold on
% pause(0.1)
end
a=200;
b=400;

figure
plot3(Pr(a:b,1),[Pr(a:b,2)],[Pr(a:b,3)])
hold on
%取中间一段螺旋轨迹作图 首位的快速移动忽略
for k=200:400
Rr=Rrv2*[input_traj(k,1:3)' input_traj(k,5:7)' input_traj(k,9:11)'];
r=[0;0;0.01];
r=Rr*r;
plot3([Pr(k,1)-r(1);Pr(k,1)],[Pr(k,2)-r(2);Pr(k,2)],[Pr(k,3)-r(3); Pr(k,3)],'b-',Pr(k,1),Pr(k,2),Pr(k,3),'ro','markersize',5)
hold on
xlabel('x')
% pause(0.05)
grid on
end

%%generate xyzabc time series for traj plan
ts=0.1;
Tall=ts*(b-a+1);
Ponline=RPY(:,1:3); %xyz坐标
Qonline=Quat;%abc朝向四元数
T{k};%T序列
end

