T0=rtm_fkine(robot_sdh,[0 0 0 0 0 -pi/5]);
T1=rtm_fkine(robot_sdh,[0 0 0 0 0 pi/5]);
R0=t2r(T0);
R1=t2r(T1);
P0=T0(1:3,end);
P1=T1(1:3,end);
[qua0]=rtm_r2angvec(R0);%quaternion
[qua1]=rtm_r2angvec(R1);
Nstep=100;
px=linspace(P0(1),P1(1),Nstep);
py=linspace(P0(2),P1(2),Nstep);
pz=linspace(P0(3),P1(3),Nstep);
Pi_position=[px' py' pz'];
Pi_orientation=rtm_Slerp(qua0,qua1,Nstep-1);
% for i=1:Nstep
%     R(:,:,i)=rtm_angvec2r(Pi_orientation(i,:));
%     q(i,:)=rtm_ikine(robot_sdh,r2t(R(:,:,i)),1,1,1,0);
% end
% robot_sdh.plot(robot_sdh.ikine(r2t(R)))
% robot_sdh.plot(robot_sdh.ikine(T1))
% q=robot_sdh.ikine(r2t(R));

 for i = 1:100
Rr(:,:,i)=rtm_angvec2r(Pi_orientation(i,:));
Prpy(i,:)=rtm_r2xyzabc(r2t(Rr(:,:,i)));
 end

%% test result
Pi_orientation=Qin_part;
Pi_position=zeros(200,3);
figure
myCoordinateFrame(Pi_position,Pi_orientation)



function myCoordinateFrame(Pi_position,Pi_orientation)
% Pi_position:Nx3 column array;
% Pi_orientation:Nx3 column array，euler angle
%             or Nx4 quaternion with unitization;
%
if size(Pi_orientation,2)== 4    %if input is quaternion
    Pi_orientation=quat2eul(Pi_orientation);
end
rotmZYX = eul2rotm(Pi_orientation);  %if input is RPY
ox=Pi_position(:,1);
oy=Pi_position(:,2);
oz=Pi_position(:,3);
ux=squeeze(rotmZYX(1,1,:));vx=squeeze(rotmZYX(1,2,:));wx=squeeze(rotmZYX(1,3,:));
uy=squeeze(rotmZYX(2,1,:));vy=squeeze(rotmZYX(2,2,:));wy=squeeze(rotmZYX(2,3,:));
uz=squeeze(rotmZYX(3,1,:));vz=squeeze(rotmZYX(3,2,:));wz=squeeze(rotmZYX(3,3,:));
% AXhandle = quiver3(ox, oy, oz, ux, vx, wx,  'r', 'ShowArrowHead','on', 'MaxHeadSize', 0.999999, 'AutoScale', 'on');hold on;
% AYhandle = quiver3(ox, oy, oz, uy, vy, wy,  'g', 'ShowArrowHead','on', 'MaxHeadSize', 0.999999, 'AutoScale', 'on');hold on;
% AZhandle = quiver3(ox, oy, oz, uz, vz, wz,  'b', 'ShowArrowHead','on', 'MaxHeadSize', 0.999999, 'AutoScale', 'on');grid on;
% AXhandle.LineWidth=1.5;
% AYhandle.LineWidth=1.5;
% AZhandle.LineWidth=1.5;
% view (60,45);
%
% set the coordinate display one by one
Num=size(Pi_orientation);
for i=1:1:Num
    AXhandle = quiver3(ox(i), oy(i), oz(i), ux(i), vx(i), wx(i),  'r', 'ShowArrowHead','on', 'MaxHeadSize', 0.39, 'AutoScale', 'on');hold on;
    AYhandle = quiver3(ox(i), oy(i), oz(i), uy(i), vy(i), wy(i),  'g', 'ShowArrowHead','on', 'MaxHeadSize', 0.39, 'AutoScale', 'on');hold on;
    AZhandle = quiver3(ox(i), oy(i), oz(i), uz(i), vz(i), wz(i),  'b', 'ShowArrowHead','on', 'MaxHeadSize', 0.39, 'AutoScale', 'on');
    AXhandle.LineWidth=1.5;
    AYhandle.LineWidth=1.5;
    AZhandle.LineWidth=1.5;
    pause(0.01);
end
fl=legend([ AXhandle, AYhandle, AZhandle],{'Y','P','R'},'FontAngle','italic','Box','off');
end
