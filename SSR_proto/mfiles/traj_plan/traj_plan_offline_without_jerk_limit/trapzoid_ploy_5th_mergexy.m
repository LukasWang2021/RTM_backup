%% 整体线段长进行梯形速度规划 然后分解到xy轴
%% 准备给定路径 设定总时长 插值 
fs=0.01;%原给定轨迹采样间隔
tn=[0:fs:30]; %此时间无用，仅用于生成路径
VPx=sin(0.2*pi*tn)+0.1*rand(1,length(tn))+sin(0.5*pi*tn);%路径生成
VPy=cos(0.2*pi*tn)+0.1*rand(1,length(tn))+cos(0.5*pi*tn);
fx=fit(tn',VPx','smoothingspline','SmoothingPara',0.999);%xyz曲线拟合
fy=fit(tn',VPy','smoothingspline','SmoothingPara',0.999);
tnew=tn(1):fs:tn(end);%重新采样，等效于10倍频率插值。
px=fx(tnew);
py=fy(tnew);

%% 梯形轨迹规划 此段也可以用五阶多项式的始末条件构建等式来求解
N=length(tnew);
sxy=0;
t_tar=30;%规划总时间
ka=2/3;%加减速度段所占时间比例
for i=1:length(px)-1
    delx=abs(px(i+1)-px(i));
    dely=abs(py(i+1)-py(i));
    delxy=sqrt(delx^2+dely^2);
    sxy=sxy+delxy;%求总行程绝对距离
end
v_tar=sxy/t_tar*(1/(1-ka/2));%计算匀速段速度;
t0=0;
t1=t0+ka/2*t_tar;%加速时长
t2=t1+t_tar*(1-ka);
t3=t_tar;
a=v_tar/(t1-t0);
%分段
p0=0;
p1=v_tar/2*(t1-t0);%加速段结束点位绝对距离
p2=p1+v_tar*(t2-t1);
p3=sxy(end);
v0=0;
v1=v_tar;
acc0=a;
acc1=a;
t0=t0;
t1=t1;

fs=0.001;
pos_give=0;%初始化
for i=1:N-1
delx=abs(px(i+1)-px(i));
dely=abs(py(i+1)-py(i));
delxy=sqrt(delx^2+dely^2);
pos_give=pos_give+delxy%求当前点位所在的总行程长度
[pos,vel,acc,txy_set]=poly5(p0,p1,v0,v1,acc0,acc1,t0,t1,0.001,pos_give);
    if txy_set==ka*t_tar/2 %匀速段起始时间点
        [pos,vel,acc,txy_set]=poly5(p1,p2,v1,v1,0,0,t1,t2,0.001,pos_give);
        if txy_set==ka*t_tar %减速段起始时间点
            [pos,vel,acc,txy_set]=poly5(p2,p3,v1,v0,-acc0,-acc1,t2,t3,0.001,pos_give);
        end
    end
txy(i)=txy_set;
end

figure(1) % 原轨迹的各自由度轨迹
clf
subplot(2,1,1)
hold on
plot(txy,px(1:i))
plot(tnew,px)
legend('新规划轨迹','原轨迹')
subplot(2,1,2)
hold on
plot(txy,py(1:i))
plot(tnew,py)
% 
% figure(2) %新规划的合线速度和原本速度对比
% hold on
% for n=1:N-1
% delx=abs(px(n+1)-px(n));
% dely=abs(py(n+1)-py(n));
% delxy(n)=sqrt(delx^2+dely^2);
% plot(n,delxy(n)/(txy(n+1)-txy(n)),'bo')
% % plot(n,delxy(n)/(tnew(n+1)-tnew(n)),'bo')
% end

% figure(3)
% cla
% a=plot(0,0);
% axis([min(px),max(px),min(py),max(py)]);hold on
% for n=1:i-1
%    plot(px(n),py(n),'rx');
%    pause(txy(n+1)-txy(n))
% %    pause(tnew(n+1)-tnew(n));
% end

%插值重采样
fx_new=fit(txy',px,'smoothingspline','SmoothingPara',0.99999);
fy_new=fit(txy',py

,'smoothingspline','SmoothingPara',0.99999);
x=fx_new([0:0.01:t_tar]);
y=fy_new([0:0.01:t_tar]);

for i=1:length(x)-1
delx=abs(x(i+1)-x(i));
dely=abs(y(i+1)-y(i));
delxy=sqrt(delx^2+dely^2);
sxy(i+1)=sxy(i)+delxy;%求总行程绝对距离
end
plot(sxy)


%% 5阶轨迹插值
%  [pos,vel,acc,txy_set]=poly5(p2,p3,v1,v0,-acc0,-acc1,t2,t3,0.001,pos_give);
 [pos,vel,acc,txy_set]=poly5(p0,p1,v0,v1,acc0,acc0,t1,t2,0.001,5.5);
function [pos,vel,acc,t_set]=poly5(p0,p1,v0,v1,acc0,acc1,t0,t1,fs,pos_give)%fs采样间隔 pos_give当前绝对位置，txy_set，反回新的对应时间
T=t1-t0;
a0=p0;
a1=v0;
a2=acc0/2;
a3=-(20*p0 - 20*p1 + 12*T*v0 + 8*T*v1 + 3*T^2*acc0 - T^2*acc1)/(2*T^3);
a4=(30*p0 - 30*p1 + 16*T*v0 + 14*T*v1 + 3*T^2*acc0 - 2*T^2*acc1)/(2*T^4);
a5=-(12*p0 - 12*p1 + 6*T*v0 + 6*T*v1 + T^2*acc0 - T^2*acc1)/(2*T^5);

%轨迹生成

% syms t
% t_set=solve(pos_give==a0+a1*power((t-t0),1)+a2*power((t-t0),2)+a3*power((t-t0),3)+...
%     a4*power(t-t0,4)+a5*power(t-t0,5),t,'Real',true);
% t_set=eval(vpa(t_set));t_set=t_set(t_set>t0&t_set<t1);%找到在规划时间段内的唯一解。
%位置
t=t0:fs:t1;
pos=a0+a1*power((t-t0),1)+a2*power((t-t0),2)+a3*power((t-t0),3)+...
    a4*power(t-t0,4)+a5*power(t-t0,5);  
n=find(pos<=pos_give);
t_set=t(n(end));
%速度
vel=a1+2*a2*power((t-t0),1)+3*a3*power((t-t0),2)+4*a4*power(t-t0,3)+...
    5*a5*power(t-t0,4);
% 加速度
acc=2*a2+6*a3*power((t-t0),1)+12*a4*power(t-t0,2)+20*a5*power(t-t0,3);
% vel=0;
% acc=0;

% 绘图
% subplot(3,1,1)
% plot(t,pos,'r');
% ylabel('position')
% grid on
% subplot(3,1,2)
% plot(t,vel,'b');
% ylabel('velocity')
% grid on
% subplot(3,1,3)
% plot(t,acc,'g');
% xlabel('(b)');
% ylabel('acceleration')
% grid on
end