%% use spapi to create Bspline and use fnplot to plot spline
tn=[0:0.01:9.9];
VP=0.1*sin(2*pi*tn+pi/2)+0.1*rand(1,length(tn));
h_interval_init=1/(length(VP)-1);
knots=[0:h_interval_init:1];
h_init=knots;

f=spapi([0 0 0 0 0 0 0.5 1 1.5 2 2 2 2 2 2],[0 0 0 0.5 1 1.5 2 2 2],[0 0 10 0.1 0.5 0.1 -0.1 0 10]);


f=spapi([0 0 0 0 0 h_init 1 1 1 1 1],[0 0 h_init 1 1],[0 0 10 VP(2:end) 0 10]);
% df = fnder(f,1); dddf = fnder(f,3);  % derivatives 
% v0 = fnval(df,[0:0.01:1])
% a0 = fnval(dddf,0)

% fnplt(f)
y=fnval(f,[0:0.0001:1]);
plot(h_init,VP,'*');hold on
plot([0:0.0001:1],y);

% t=[0 0.15 0.5 0.85 1];
% x=[0 0.1 0.5 0.1 -0.1];
% plot(t,x,'*')
% plot([0 0 0 0.25 0.5 0.75 1 1 1],f.coefs,'o')


%% test 
kT=0;
kJ=1;
tn=[0:0.1:5];
VP=0.1*sin(0.2*pi*tn)+0.01*rand(1,length(tn));
f=fit(tn',VP','smoothingspline','SmoothingPara',0.999);
plot(f,tn,VP)

VP_smooth=f(tn)';   
h_interval_init=tn(end)/(length(VP_smooth)-1);
knots=[0:h_interval_init:tn(end)]; %knots就等于tn
h_init=knots;
f=spapi([0 0 0 0 0 0 0 knots knots(end)*ones(1,7)],[0 0 0 h_init h_init(end) h_init(end) h_init(end)],[VP_smooth(1) 0 0 0 VP_smooth(2:end) 0 0 0]);
ts=[0:0.001:tn(end)];
y=fnval(f,ts);
df = fnder(f,1); ddf=fnder(f,2);dddf = fnder(f,3);  % derivatives 
v0 = fnval(df,ts);
a0 = fnval(ddf,ts);
j0 = fnval(dddf,ts);
figure
plot(h_init,VP_smooth,'*');hold on
plot(ts,y);
plot(ts,v0)
plot(ts,a0)
plot(ts,j0)
% VPx=VP_smooth(1:5:end);
% VPy=VP_smooth(1:5:end);

%% 
optimum_func = @(parameters)costfuncxy(parameters,VP_smooth,ts,kT,kJ);%parameters=time_interval
options = optimoptions(@lsqnonlin,'MaxIterations',5000,'MaxFunctionEvaluations',1000);
h_interval_init=tn(end)/(length(VP_smooth)-1);
h_interval_init=h_interval_init*ones(1,length(tn)-1);
lb=0.1*h_interval_init;
ub=20*h_interval_init;
h_optimum = lsqnonlin(optimum_func,h_interval_init,lb,ub,options);
%% 
J=plot_opt(h_optimum,VP_smooth,ts);

function J=costfuncxy(h_interval,VP,ts,kT,kJ)

for n=1:length(h_interval)+1
   if n==1
       h(n)=0; % 时间从0开始 每次递增hi
   else
       h(n)=h(n-1)+h_interval(n-1);
   end
end   
h_interval_new=h(end)/(length(VP)-1);
knots=[0:h_interval_new:h(end)]; %knots就等于tn
f=spapi([0 0 0 0 0 0 0 knots knots(end)*ones(1,7)],[0 0 0 h h(end) h(end) h(end)],[VP(1) 0 0 0 VP(2:end) 0 0 0]);
df = fnder(f,1); ddf=fnder(f,2);dddf = fnder(f,3);  % derivatives 
v0 = fnval(df,ts);
a0 = fnval(ddf,ts);
j0 = fnval(dddf,ts);
% for i = 1:length(x)-1
%     k(i)=norm([x(i+1)-x(i),y(i+1)-y(i)]);
% end
J=abs(j0);
sum(J);
end


function J=plot_opt(h_optimum,VP,ts)

for n=1:length(h_optimum)+1
   if n==1
       h(n)=0; % 时间从0开始 每次递增hi
   else
       h(n)=h(n-1)+h_optimum(n-1);
   end
end   
h_interval_new=h(end)/(length(VP)-1);
knots=[0:h_interval_new:h(end)]; %knots就等于tn
f=spapi([0 0 0 0 0 0 0 knots knots(end)*ones(1,7)],[0 0 0 h h(end) h(end) h(end)],[VP(1) 0 0 0 VP(2:end) 0 0 0]);
y=fnval(f,ts*1.1);
df = fnder(f,1); ddf=fnder(f,2);dddf = fnder(f,3);  % derivatives 
v0 = fnval(df,ts*1.1);
a0 = fnval(ddf,ts*1.1);
j0 = fnval(dddf,ts*1.1);
J=abs(j0);
figure
plot(h,VP,'*');hold on
plot(ts*1.1,y);
% plot(ts,v0)
% plot(ts,a0)
% plot(ts,j0)
end