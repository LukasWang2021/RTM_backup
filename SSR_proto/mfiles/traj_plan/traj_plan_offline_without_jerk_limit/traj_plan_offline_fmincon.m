%%Trajectory plan offline with finmincon for jerk minimization. Input T
%%from 3D visual space and output xyzabc/T in robot work space.

%% Input prepare
try
robot_sdh=evalin('base','robot_sdh');
catch
addpath(genpath('D:\实时侠同步\RTM_data\rtm_mfiles'))
run('rtm_robot_sdh.m')
end
[Pin,Qin,Tin]=traj_generate('out4.txt');
a=200;
b=400;%截取部分轨迹
fs=0.1;
Pin_part=Pin(a:b,:);
Qin_part=Qin(a:b,:);
N=length(Pin_part);
tn=0:fs:(b-a)*fs;
VPx=Pin_part(:,1);
VPy=Pin_part(:,2);
VPz=Pin_part(:,3);

%smooth traj with smoothspline and smooth_quat
kf=0.999;%fit rate 1 = original curve
fx=fit(tn',VPx,'smoothingspline','SmoothingPara',kf);
fy=fit(tn',VPy,'smoothingspline','SmoothingPara',kf);
fz=fit(tn',VPz,'smoothingspline','SmoothingPara',kf);
px=fx(tn);
py=fy(tn);
pz=fz(tn);
Qs=smooth_quat(Qin_part,7);

%% optimize jerk with fmincon by chaning time interval between VPs
Ttar=20;
optimum_func = @(parameters)costfuncxy(parameters,px,py,pz);%parameters=time_interval
options = optimoptions(@fmincon,'MaxIterations',1000,'MaxFunctionEvaluations',50*N,'Display','iter','Algorithm','sqp');
dt0=fs*ones(1,length(tn)-1);
lb=0.1*fs*ones(1,length(tn)-1);%修改优化过程中VP间隔时间变化的上下限制
ub=10*fs*ones(1,length(tn)-1);
[dt_opt,J,~,report] = fmincon(optimum_func,dt0,ones(1,length(dt0)),Ttar,[],[],lb,ub,[],options); %使用的时间<=Ttar
tnew=zeros(1,N);
for n=1:N-1
tnew(n+1)=tnew(n)+dt_opt(n);
end
figure
plot(tn,VPz,'kx',tn,pz,'b-',tnew,pz,'r-')
title('Z原输入点 平滑后输入点 优化后轨迹')

%% interpolation with smooth spline/spline and Slerp
fx=fit(tnew',px,'smoothingspline','SmoothingPara',0.99999); %新的数据点应该尽可能的贴近所有平滑后的点，所以这里的拟合度趋近于1
fy=fit(tnew',py,'smoothingspline','SmoothingPara',0.99999);
fz=fit(tnew',pz,'smoothingspline','SmoothingPara',0.99999);
fsample=0.01;
tn_new=[0:fsample:tnew(end)];%重采样频率
dt_new=fsample*ones(1,length(tn_new)-1);
px_new=fx(tn_new);
py_new=fy(tn_new);
pz_new=fz(tn_new);
Qnew=[];
Tnew=[];
i=1;
for t=tn_new
    [~,ind]=find(t>=tnew);%判断在哪个tnew区间
    t0=tnew(ind(end));%起点时间
    t1=tnew(ind(end)+1);%终点时间
    tc=(t-t0)/(t1-t0);%当前插值点
%     Qnew(i,:)=rtm_Slerpt(Qs(ind(end),:),Qs(ind(end)+1,:),tc);
    Qnew(i,:)=quatinterp(Qs(ind(end),:),Qs(ind(end)+1,:),tc,'slerp');
    Rnew=rtm_angvec2r(Qnew(i,:));
    Tnew(:,:,i)=[Rnew(1,:) px_new(i);
                  Rnew(2,:) py_new(i);
                  Rnew(3,:) pz_new(i);
                  0 0 0 1];
    i=i+1;
end

q=[];

for i = 1:2000
    q(i,:)=rtm_ikine(robot_sdh,Tnew(:,:,i)*[-1 0 0 0;0 -1 0 0;0 0 1 0;0 0 0 1],1,1,1,0);
    Tnew_trans=Tnew(:,:,i)*[-1 0 0 0;0 -1 0 0;0 0 1 0;0 0 0 1];
end
figure
robot_sdh.plot(q,'trail','b','fps',100);
ta=table(px_new',py_new',pz_new',c',b',a','VariableNames',{'x','y','z','c','b','a'}); 
writetable(ta, 'traj_plan_rectangle.csv');
traj_plot(Tnew,tn_new,VPx,VPy,VPz,tn,0);


function J=costfuncxy(dt,px,py,pz)
kT=0;%损失函数中时间占比系数
N=length(dt);
tnew=zeros(N+1,1);
for n=1:N
tnew(n+1)=tnew(n)+dt(n);
end
vx=(px(2:end)-px(1:end-1))./(tnew(2:end)-tnew(1:end-1));
ax=(vx(2:end)-vx(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
vy=(py(2:end)-py(1:end-1))./(tnew(2:end)-tnew(1:end-1));
ay=(vy(2:end)-vy(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
vz=(pz(2:end)-pz(1:end-1))./(tnew(2:end)-tnew(1:end-1));
az=(vz(2:end)-vz(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
J=sum(((ax.^2+ay.^2+az.^2)))+kT*sum(dt);
end

