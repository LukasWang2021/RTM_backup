%% use spapi to create Bspline and use fnplot to plot spline
tn=[0:0.01:9.9];
VP=0.1*sin(2*pi*tn+pi/2)+0.1*rand(1,length(tn));
h_interval_init=1/(length(VP)-1);
knots=[0:h_interval_init:1];
h_init=knots;

fx=spapi([0 0 0 0 0 0 0.5 1 1.5 2 2 2 2 2 2],[0 0 0 0.5 1 1.5 2 2 2],[0 0 10 0.1 0.5 0.1 -0.1 0 10]);


fx=spapi([0 0 0 0 0 h_init 1 1 1 1 1],[0 0 h_init 1 1],[0 0 10 VP(2:end) 0 10]);
% df = fnder(f,1); dddf = fnder(f,3);  % derivatives 
% v0 = fnval(df,[0:0.01:1])
% a0 = fnval(dddf,0)

% fnplt(f)
xx=fnval(fx,[0:0.0001:1]);
plot(h_init,VP,'*');hold on
plot([0:0.0001:1],xx);

% t=[0 0.15 0.5 0.85 1];
% x=[0 0.1 0.5 0.1 -0.1];
% plot(t,x,'*')
% plot([0 0 0 0.25 0.5 0.75 1 1 1],f.coefs,'o')


%% test 
kT=0;
kJ=1;
tn=[0:0.05:5];
VPx=sin(0.2*pi*tn)+0.1*rand(1,length(tn));
VPy=cos(0.2*pi*tn)+0.1*rand(1,length(tn));
fx=fit(tn',VPx','smoothingspline','SmoothingPara',0.999);
fy=fit(tn',VPy','smoothingspline','SmoothingPara',0.999);
% plot(fx,tn,VPx)
% plot(fy,tn,VPy)
figure(1)
plot(VPx,VPy,'x');hold on
plot(fx(tn),fy(tn));

VPx_smooth=fx(tn)';
VPy_smooth=fy(tn)';
h_interval_init=tn(end)/(length(VPx_smooth)-1);
knots=[0:h_interval_init:tn(end)]; %knots就等于tn
h_init=knots;
fx=spapi([0 0 0 0 0 0 0 knots knots(end)*ones(1,7)],[0 0 0 h_init h_init(end) h_init(end) h_init(end)],[VPx_smooth(1) 0 0 0 VPx_smooth(2:end) 0 0 0]);
fy=spapi([0 0 0 0 0 0 0 knots knots(end)*ones(1,7)],[0 0 0 h_init h_init(end) h_init(end) h_init(end)],[VPy_smooth(1) 0 0 0 VPy_smooth(2:end) 0 0 0]);
ts=[0:0.001:tn(end)];%拟合后离散采样点
xx=fnval(fx,ts);
yy=fnval(fy,ts);
dfx = fnder(fx,1); ddfx=fnder(fx,2);dddfx = fnder(fx,3);  % derivatives 
vx = fnval(dfx,ts);
ax = fnval(ddfx,ts);
jx = fnval(dddfx,ts);
dfy = fnder(fy,1); ddfy=fnder(fy,2);dddfy = fnder(fy,3);  % derivatives 
vy = fnval(dfy,ts);
ay = fnval(ddfy,ts);
jy = fnval(dddfy,ts);
figure(2)
subplot(2,1,1)
plot(h_init,VPx_smooth,'*');hold on
plot(ts,xx);
title('优化前曲线x')
subplot(2,1,2)
plot(h_init,VPy_smooth,'*');hold on
plot(ts,yy);
title('优化前曲线y')
figure(1)
plot(xx,yy);
title('初始轨迹')
legend('输入点','拟合曲线','初始规划（优化前）')
figure(3)
subplot(2,1,1)
plot(ts,vx);hold on
plot(ts,ax)
plot(ts,jx)
title('优化前x曲线')
subplot(2,1,2)
plot(ts,vy);hold on
plot(ts,ay)
plot(ts,jy)
title('优化前y曲线')
% VPx=VP_smooth(1:5:end);
% VPy=VP_smooth(1:5:end);

%% 
optimum_func = @(parameters)costfuncxy(parameters,VPx_smooth,VPy_smooth,ts,kT,kJ);%parameters=time_interval
options = optimoptions(@lsqnonlin,'MaxIterations',10000,'MaxFunctionEvaluations',1000);
h_interval_init=tn(end)/(length(VPx_smooth)-1);
h_interval_init=h_interval_init*ones(1,length(tn)-1);
lb=0.1*h_interval_init;
ub=20*h_interval_init;
h_optimum = lsqnonlin(optimum_func,h_interval_init,lb,ub,options);
tk=[0:0.001:ts(end)*1.1];%延长显示的时间
[xxn,yyn]=plot_opt(h_optimum,VPx_smooth,VPy_smooth,tk);

function J=costfuncxy(h_interval,VPx,VPy,ts,kT,kJ)

for n=1:length(h_interval)+1
   if n==1
       h(n)=0; % 时间从0开始 每次递增hi
   else
       h(n)=h(n-1)+h_interval(n-1);
   end
end   
h_interval_new=h(end)/(length(VPx)-1);
knots=[0:h_interval_new:h(end)]; %knots就等于tn
fx=spapi([0 0 0 0 0 0 0 knots knots(end)*ones(1,7)],[0 0 0 h h(end) h(end) h(end)],[VPx(1) 0 0 0 VPx(2:end) 0 0 0]);
fy=spapi([0 0 0 0 0 0 0 knots knots(end)*ones(1,7)],[0 0 0 h h(end) h(end) h(end)],[VPy(1) 0 0 0 VPy(2:end) 0 0 0]);
dfx = fnder(fx,1); ddfx=fnder(fx,2);dddfx = fnder(fx,3);  % derivatives 
dfy = fnder(fy,1); ddfy=fnder(fy,2);dddfy = fnder(fy,3);  % derivatives 
vx = fnval(dfx,ts);
ax = fnval(ddfx,ts);
jx = fnval(dddfx,ts);
vy = fnval(dfy,ts);
ay = fnval(ddfy,ts);
jy = fnval(dddfy,ts);
% for i = 1:length(x)-1
%     k(i)=norm([x(i+1)-x(i),y(i+1)-y(i)]);
% end
J=abs(jx)+abs(jy);
sum(J)
end


function [xxn,yyn]=plot_opt(h_optimum,VPx,VPy,tk)

for n=1:length(h_optimum)+1
   if n==1
       h(n)=0; % 时间从0开始 每次递增hi
   else
       h(n)=h(n-1)+h_optimum(n-1);
   end
end   
h_interval_new=h(end)/(length(VPx)-1);
knots=[0:h_interval_new:h(end)]; %knots就等于tn
fx=spapi([0 0 0 0 0 0 0 knots knots(end)*ones(1,7)],[0 0 0 h h(end) h(end) h(end)],[VPx(1) 0 0 0 VPx(2:end) 0 0 0]);
fy=spapi([0 0 0 0 0 0 0 knots knots(end)*ones(1,7)],[0 0 0 h h(end) h(end) h(end)],[VPy(1) 0 0 0 VPy(2:end) 0 0 0]);
xxn=fnval(fx,tk);
yyn=fnval(fy,tk);
dfx = fnder(fx,1); ddfx=fnder(fx,2);dddfx = fnder(fx,3);  % derivatives 
dfy = fnder(fy,1); ddfy=fnder(fy,2);dddfy = fnder(fy,3);  % derivatives 
vx= fnval(dfx,tk);
ax = fnval(ddfx,tk);
jx = fnval(dddfx,tk);
vy= fnval(dfy,tk);
ay = fnval(ddfy,tk);
jy = fnval(dddfy,tk);
figure(4)
subplot(2,1,1)
plot(tk,vx,tk,ax,tk,jx)
title('优化后x曲线')
subplot(2,1,2)
plot(tk,vy,tk,ay,tk,jy)
title('优化后y曲线')
% figure(5)
% plot(h,VPx,'*');hold on
% plot(h,VPy,'*');hold on
% plot(ts,xx);
% plot(ts,yy);
figure(1)
plot(xxn,yyn)
legend('输入点','拟合曲线','初始规划','最终规划')
% plot(ts,v0)
% plot(ts,a0)
% plot(ts,j0)
end