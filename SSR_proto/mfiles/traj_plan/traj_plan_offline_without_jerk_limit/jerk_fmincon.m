try
robot_sdh=evalin('base','robot_sdh');
catch
addpath(genpath('D:\实时侠同步\RTM_data\mytest_SSR'))
run('rtm_startup.m')
end
fs=0.01;
tn=[0:fs:5];
Ttar=6;%设计最长规划时间
kf=0.9999;%拟合精度
initial_pos=[0 0 0 0 -pi/2 0];
init_T=rtm_fkine(robot_sdh,initial_pos);
k=0.1;%比例缩放系数
x0=init_T(1,4);
y0=init_T(2,4);
z0=init_T(3,4);
VPx=x0+0.05*tn(1:floor(length(tn)/2));
VPx=[VPx VPx(end).*ones(1,ceil(length(tn)/2))];
VPy=y0*ones(1,length(tn));
VPz=z0*ones(1,floor(length(tn)/2));
VPz=[VPz z0-0.1*(tn(ceil(length(tn)/2):end)-tn(floor(length(tn)/2)))];

%%轨迹2 圆滑曲线
% VPx=x0-k*sin(0.2*pi*tn)+0.01*rand(1,length(tn))+k*sin(0.5*pi*tn);
% VPy=y0+k*cos(0.2*pi*tn)+0.01*rand(1,length(tn))-k*cos(0.5*pi*tn);
% VPz=z0-0.1*tn+0.01*rand(1,length(tn));
% px=VPx;
% py=VPy;
% pz=VPz;
% VPx=VPx+0.005*rand(1,length(VPx));%加入噪音
VPz=VPz+0.001*rand(1,length(VPz));
fx=fit(tn',VPx','smoothingspline','SmoothingPara',kf);
fy=fit(tn',VPy','smoothingspline','SmoothingPara',kf);
fz=fit(tn',VPz','smoothingspline','SmoothingPara',kf);
px=fx(tn)';
py=fy(tn)';
pz=fz(tn)';



%得到新的时间规划dt_opt 累加即tnew
optimum_func = @(parameters)costfuncxy(parameters,px,py,pz);%parameters=time_interval
options = optimoptions(@fmincon,'MaxIterations',100000,'MaxFunctionEvaluations',10000);
dt0=fs*ones(1,length(tn)-1);
lb=fs*ones(1,length(tn)-1);%修改优化过程中VP间隔时间变化的上下限制
ub=Ttar/(length(tn))*ones(1,length(tn)-1);
ub=5*lb;
dt_opt = fmincon(optimum_func,dt0,ones(1,length(dt0)),Ttar,[],[],lb,ub,[],options);

N=length(dt_opt);
tnew=zeros(1,N+1);
for n=1:N
tnew(n+1)=tnew(n)+dt_opt(n);
end
plot(tn,VPz,'kx',tn,pz,'.b',tnew,pz,'rx')
title('原输入点 平滑后输入点 优化后轨迹')

%根据tnew重新拟合曲线重采样
fx=fit(tnew',px','smoothingspline','SmoothingPara',0.99999); 
fy=fit(tnew',py','smoothingspline','SmoothingPara',0.99999);
fz=fit(tnew',pz','smoothingspline','SmoothingPara',0.99999);
tn_new=[0:fs:tnew(end)];
dt_new=fs*ones(1,length(tn_new)-1);
px_new=fx(tn_new)';
py_new=fy(tn_new)';
pz_new=fz(tn_new)';


%% PLOT
[vx,vy,vz,ax,ay,az]=myplot(dt_new,px_new,py_new,pz_new);
[vxo,vyo,vzo,axo,ayo,azo]=myplot(dt0,px,py,pz);
figure(2)
subplot(3,1,1)
plot(tn(1:end-2),axo)
hold on;plot(tn_new(1:end-2),ax)
subplot(3,1,2)
plot(tn(1:end-2),ayo)
hold on;plot(tn_new(1:end-2),ay)
subplot(3,1,3)
plot(tn(1:end-2),azo)
hold on;plot(tn_new(1:end-2),az)
figure(1)
plot3(VPx,VPy,VPz,'x');hold on 
plot3(fx(tn_new)',fy(tn_new)',fz(tn_new)','o')
grid on
title('优化前后空间轨迹')

% traj_plot(px_new,py_new,pz_new,px_new,py_new,pz_new,tn_new,VPx,VPy,VPz,tn);

 
function J=costfuncxy(dt,px,py,pz)
N=length(dt);
tnew=zeros(1,N+1);
for n=1:N
tnew(n+1)=tnew(n)+dt(n);
end
vx=(px(2:end)-px(1:end-1))./(tnew(2:end)-tnew(1:end-1));
ax=(vx(2:end)-vx(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
vy=(py(2:end)-py(1:end-1))./(tnew(2:end)-tnew(1:end-1));
ay=(vy(2:end)-vy(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
vz=(pz(2:end)-pz(1:end-1))./(tnew(2:end)-tnew(1:end-1));
az=(vz(2:end)-vz(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
J=mean(((ax.^2+ay.^2+az.^2)));
end

function [c,ceq] = timecon(x)
c = sum(x);
ceq = [];
end

function [vx,vy,vz,ax,ay,az]=myplot(dt,px,py,pz)
N=length(dt);
tnew=zeros(1,N+1);
for n=1:N
tnew(n+1)=tnew(n)+dt(n);
end
vx=(px(2:end)-px(1:end-1))./(tnew(2:end)-tnew(1:end-1));
ax=(vx(2:end)-vx(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
vy=(py(2:end)-py(1:end-1))./(tnew(2:end)-tnew(1:end-1));
ay=(vy(2:end)-vy(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
vz=(pz(2:end)-pz(1:end-1))./(tnew(2:end)-tnew(1:end-1));
az=(vz(2:end)-vz(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
end