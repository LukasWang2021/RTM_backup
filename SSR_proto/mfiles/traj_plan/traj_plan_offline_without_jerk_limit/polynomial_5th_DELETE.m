%% direct interpolation with 3/5 order polynomial unvalid version!!
%% input prepar
fs=0.1;
tn=[0:fs:30];
VPx=sin(0.2*pi*tn)+0.1*rand(1,length(tn))+sin(0.5*pi*tn);
VPy=cos(0.2*pi*tn)+0.1*rand(1,length(tn))+cos(0.5*pi*tn);
fx=fit(tn',VPx','smoothingspline','SmoothingPara',0.999);
fy=fit(tn',VPy','smoothingspline','SmoothingPara',0.999);
px=fx(tn);
py=fy(tn);
%% segements:acc avgvel dec
vm=1;%max vel
am=2;%max acc
% for n=1:length(tn)-1
%    vx(n)=(fx(tn(n+1))-fx(tn(n)))/ts;
%    vy(n)=(fy(tn(n+1))-fy(tn(n)))/ts;
%    if vx(n)^2+vy(n)^2>vm^2
%        vx(n)=vm*(vx/(abs(vx(n))+abs(vy(n))));
%        vy(n)=vm*(vy/(abs(vx(n))+abs(vy(n))));
%    end
% end
% vx=[0 vx(1:end-1) 0];
% vy=[0 vy(1:end-1) 0];

p=[];
v=[];
a=[];
vx=zeros(1,length(tn));
ax=zeros(1,length(tn));
vx(1:100)=am*fs*tn(1:100);
ax(1:100)=am;
vx(101:end-100)=vx(100);
ax(101:end-100)=0;
ax(end-99:end)=-am;
vx(end-99:end)=fliplr(vx(1:100));
vx=ones(1,length(tn));
ax=zeros(1,length(tn));
for i=1:length(tn)-1
vx(i)=vx(i)*sign(px(i+1)-px(i));
tn(i+1)=tn(i)+(px(i+1)-px(i))/vx(i);
[pos,vel,acc]=poly5(px(i),px(i+1),vx(i),vx(i),ax(i),ax(i+1),tn(i),tn(i+1),0.01);
p=[p pos(1:end-1)];
v=[v vel(1:end-1)];
a=[a acc(1:end-1)];
end

%% 5 order polynomial
[p,v,a]=poly5(0,-1,0,2,2,2,0,1,0.01);

function [pos,vel,acc]=poly5(p0,p1,v0,v1,acc0,acc1,t0,t1,fs)
T=t1-t0;

a0=p0;
a1=v0;
a2=acc0/2;
a3=-(20*p0 - 20*p1 + 12*T*v0 + 8*T*v1 + 3*T^2*acc0 - T^2*acc1)/(2*T^3);
a4=(30*p0 - 30*p1 + 16*T*v0 + 14*T*v1 + 3*T^2*acc0 - 2*T^2*acc1)/(2*T^4);
a5=-(12*p0 - 12*p1 + 6*T*v0 + 6*T*v1 + T^2*acc0 - T^2*acc1)/(2*T^5);

%轨迹生成
t=t0:fs:t1;
%位置
pos=a0+a1*power((t-t0),1)+a2*power((t-t0),2)+a3*power((t-t0),3)+...
    a4*power(t-t0,4)+a5*power(t-t0,5);
%速度
vel=a1+2*a2*power((t-t0),1)+3*a3*power((t-t0),2)+4*a4*power(t-t0,3)+...
    5*a5*power(t-t0,4);
%加速度
acc=2*a2+6*a3*power((t-t0),1)+12*a4*power(t-t0,2)+20*a5*power(t-t0,3);
% 绘图
% subplot(3,1,1)
% plot(t,pos,'r');
% ylabel('position')
% grid on
% subplot(3,1,2)
% plot(t,vel,'b');
% ylabel('velocity')
% grid on
% subplot(3,1,3)
% plot(t,acc,'g');
% xlabel('(b)');
% ylabel('acceleration')
% grid on
end

% clear;
% clc;
% syms T;
% syms p0 v0 acc0 p1 v1 acc1;
% b=[p0;v0;acc0;p1;v1;acc1];
% A=[1 0 0 0 0 0;
%     0 1 0 0 0 0;
%     0 0 2 0 0 0;
%     1 T T.^2 T.^3 T.^4 T.^5;
%     0 1 2*T 3*T.^2 4*T.^3 5*T.^4;
%     0 0 2 6*T 12*T.^2 20*T.^3];
% 
% r = pinv(A)*b;
% simple_r = simplify(r)
% a0=simple_r(1);
% a1=simple_r(2);
% a2=simple_r(3);
% a3=simple_r(4);
% a4=simple_r(5);
% a5=simple_r(6);

