%% 将5阶多项式的加减匀速段式子提取出来（梯形速度也可以直接写出来），然后直接用公式计算轨迹上绝对位置对应的时间。
%% 生成轨迹
% clear
% clc
fs=0.05;
tn=[0:fs:6];
initial_pos=[0 0 0 0 -pi/2 0];
init_T=rtm_fkine(robot_sdh,initial_pos);
k=0.1;%比例缩放系数
x0=init_T(1,4);
y0=init_T(2,4);
z0=init_T(3,4);
VPx=x0-k*sin(0.2*pi*tn)+0.01*rand(1,length(tn))+k*sin(0.5*pi*tn);
VPy=y0+k*cos(0.2*pi*tn)+0.01*rand(1,length(tn))-k*cos(0.5*pi*tn);
VPz=z0-0.1*tn+0.01*rand(1,length(tn));
fx=fit(tn',VPx','smoothingspline','SmoothingPara',0.99);
fy=fit(tn',VPy','smoothingspline','SmoothingPara',0.99);
fz=fit(tn',VPz','smoothingspline','SmoothingPara',0.99);
tnew=tn(1):fs:tn(end);%保持原输入数据精度离散采样拟合曲线
px=fx(tnew);
py=fy(tnew);
pz=fz(tnew);
%% 直接梯形规划
N=length(tnew);
sxyz=zeros(N,1);
delxyz=zeros(N-1,1);
t_tar=20;%规划总时间
ka=2/3;%加减速度段所占时间比例
for i=1:N-1 %积分求总行程长度
    delx=abs(px(i+1)-px(i));
    dely=abs(py(i+1)-py(i));
    delz=abs(pz(i+1)-pz(i));
    delxyz(i)=sqrt(delx^2+dely^2+delz^2);
    sxyz(i+1)=sxyz(i)+delxyz(i);%总行程绝对距离，即相对于起点位置的行程长度，不包括起点初始位置。
end
%计算分段参数
v0=0;
v_tar=sxyz(end)/t_tar*(1/(1-ka/2));%计算匀速段速度;
v1=v_tar;
t0=0;
t1=t0+ka/2*t_tar;%加速时长
t2=t1+t_tar*(1-ka);%匀速段t1~t2
t3=t_tar;
a=v_tar/(t1-t0);
p0=0;
p1=v_tar/2*(t1-t0);%加速段结束点位绝对距离
p2=p1+v_tar*(t2-t1);
p3=sxyz(end);

%计算每个采样点;的新规划时间，分三段计算。
txyz=zeros(N,1);
for n=1:N
   if sxyz(n)<p1  %加速段 s=0.5*a*t^2
        txyz(n)=sqrt(2*sxyz(n)/a);
   
   elseif p1<=sxyz(n) && sxyz(n)<=p2 %匀速段
        txyz(n)=t1+(sxyz(n)-p1)/v1;
   
   elseif p2<sxyz(n) && sxyz(n)<=p3 %减速度段s=v*t-0.5*a*t^2
        txyz(n)=t2+(v1-sqrt(v1^2-2*a*(sxyz(n)-p2)))/a;
   end  
end
txyz=real(txyz);

%% 重新拟合再采样
tn_new=(0:fs:t_tar);
fx_new=fit(txyz,px,'smoothingspline','SmoothingPara',0.9999);
fy_new=fit(txyz,py,'smoothingspline','SmoothingPara',0.9999);
fz_new=fit(txyz,pz,'smoothingspline','SmoothingPara',0.9999);
px_new=fx_new(tn_new);
py_new=fy_new(tn_new);
pz_new=fz_new(tn_new);

%% 作图
figure(1)%整体速度
plot(delxyz./(txyz(2:end)-txyz(1:end-1)))

figure(2) %新规划各方向分解曲线
subplot(3,1,1)
plot(tn,px,txyz,px,tn_new,px_new);
subplot(3,1,2)
plot(tn,py,txyz,py,tn_new,py_new);
subplot(3,1,3)
plot(tn,pz,txyz,pz,tn_new,pz_new);

figure(3)
plotro=subplot(6,2,[1 3 5 7 9 11]);hold on
plotw1=subplot(6,2,2);hold on;grid on;title('6关节速度')
plotw2=subplot(6,2,4);hold on;grid on
plotw3=subplot(6,2,6);hold on;grid on
plotw4=subplot(6,2,8);hold on;grid on
plotw5=subplot(6,2,10);hold on;grid on
plotw6=subplot(6,2,12);hold on;grid on;
plot3(plotro,px_new,py_new,pz_new,'r','linewidth',1.2);
plot3(plotro,VPx,VPy,VPz,'k.','Markersize',0.5);
N_new=length(tn_new);
Qt=zeros(N_new,6);
Wt=zeros(N_new,6);

 for n=1:N_new
% plot3(plotro,px_new(n),py(n),pz(n),'bx');hold on
T=[init_T(1,1:3)     px_new(n);
   init_T(2,1:3)     py_new(n);
   init_T(3,1:3)     pz_new(n);
   0     0     0     1];
Qt(n,:)=rtm_ikine(robot_sdh,T,1,1,1,0);
axes(plotro);
robot_sdh.plot(Qt(n,:),'noshadow','jointdiam',0.1,'delay',0,'fps',60);
if n>1
Wt(n,:)=(Qt(n,:)-Qt(n-1,:))/(tn_new(n)-tn_new(n-1));
end
plot(plotw1,tn_new(n),Wt(n,1),'r*');
plot(plotw2,tn_new(n),Wt(n,2),'r*');
plot(plotw3,tn_new(n),Wt(n,3),'r*');
plot(plotw4,tn_new(n),Wt(n,4),'r*');
plot(plotw5,tn_new(n),Wt(n,5),'r*');
plot(plotw6,tn_new(n),Wt(n,6),'r*');
% pause(txyz(n+1)-txyz(n))
end

