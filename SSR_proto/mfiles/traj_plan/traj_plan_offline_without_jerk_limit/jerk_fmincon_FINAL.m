try
robot_sdh=evalin('base','robot_sdh');
catch
addpath(genpath('D:\实时侠同步\RTM_data\rtm_mfiles'))
run('rtm_robot_sdh.m')
end
fs=0.01;%输入数据的采样间隔
tn=[0:fs:5];%输入数据的时长
Ttar=8;%设计最长规划时间
kf=0.999;%平滑拟合精度
initial_pos=[0 0 0 0 -pi/2 0];
init_T=rtm_fkine(robot_sdh,initial_pos);
k=0.1;%比例缩放系数
x0=init_T(1,4);
y0=init_T(2,4);
z0=init_T(3,4);

%%轨迹1 拐角曲线
% VPx=x0+0.05*tn(1:floor(length(tn)/2));
% VPx=[VPx VPx(end).*ones(1,ceil(length(tn)/2))];
% VPy=y0*ones(1,length(tn));
% VPz=z0*ones(1,floor(length(tn)/2));
% VPz=[VPz z0-0.1*(tn(ceil(length(tn)/2):end)-tn(floor(length(tn)/2)))];
% VPx=VPx+0.005*rand(1,length(VPx));%加入噪音
% VPz=VPz+0.005*rand(1,length(VPz));

%%轨迹2 圆滑曲线
VPx=x0-k*sin(0.2*pi*tn)+0.01*rand(1,length(tn))+k*sin(0.5*pi*tn);
VPy=y0+k*cos(0.2*pi*tn)+0.01*rand(1,length(tn))-k*cos(0.5*pi*tn);
VPz=z0-0.1*tn+0.005*rand(1,length(tn));

fx=fit(tn',VPx','smoothingspline','SmoothingPara',kf);
fy=fit(tn',VPy','smoothingspline','SmoothingPara',kf);
fz=fit(tn',VPz','smoothingspline','SmoothingPara',kf);
px=fx(tn)';
py=fy(tn)';
pz=fz(tn)';



%得到新的时间规划dt_opt 累加即tnew
optimum_func = @(parameters)costfuncxy(parameters,px,py,pz);%parameters=time_interval
options = optimoptions(@fmincon,'MaxIterations',1000,'MaxFunctionEvaluations',500000,'Display','iter');
dt0=fs*ones(1,length(tn)-1);
lb=0.5*fs*ones(1,length(tn)-1);%修改优化过程中VP间隔时间变化的上下限制
ub=10*fs*ones(1,length(tn)-1);
[dt_opt,J] = fmincon(optimum_func,dt0,ones(1,length(dt0)),Ttar,[],[],lb,ub,[],options); %使用的时间<=Ttar


N=length(dt_opt);
tnew=zeros(1,N+1);
for n=1:N
tnew(n+1)=tnew(n)+dt_opt(n);
end
plot(tn,VPz,'kx',tn,pz,'b-',tnew,pz,'ro')
title('原输入点 平滑后输入点 优化后轨迹')

%根据tnew重新拟合曲线重采样
fx=fit(tnew',px','smoothingspline','SmoothingPara',0.99999); %新的数据点应该尽可能的贴近所有平滑后的点，所以这里的拟合度趋近于1
fy=fit(tnew',py','smoothingspline','SmoothingPara',0.99999);
fz=fit(tnew',pz','smoothingspline','SmoothingPara',0.99999);
fsample=0.001;
tn_new=[0:fsample:tnew(end)];
dt_new=fsample*ones(1,length(tn_new)-1);
px_new=fx(tn_new)';
py_new=fy(tn_new)';
pz_new=fz(tn_new)';

% px_new=spline(tnew,[0 px 0],tn_new);
% py_new=spline(tnew,[0 py 0],tn_new);
% pz_new=spline(tnew,[0 pz 0],tn_new);
%% PLOT
[vx_new,vy_new,vz_new,ax_new,ay_new,az_new]=myplot(dt_new,px_new,py_new,pz_new);
[vxo,vyo,vzo,axo,ayo,azo]=myplot(dt0,px,py,pz);
figure(2)
subplot(3,1,1)
plot(tn(1:end-2),axo)
hold on;plot(tn_new(1:end-2),ax_new)
legend('优化前','优化后')
subplot(3,1,2)
plot(tn(1:end-2),ayo)
hold on;plot(tn_new(1:end-2),ay_new)
subplot(3,1,3)
plot(tn(1:end-2),azo)
hold on;plot(tn_new(1:end-2),az_new)
figure(1)
plot3(VPx,VPy,VPz,'x');hold on 
plot3(px_new,py_new,pz_new,'o')
grid on
title('优化前后空间轨迹')


a=pi*ones(1,length(px_new));
b=zeros(1,length(px_new));
c=zeros(1,length(px_new));
ta=table(roundn(px_new',-6),roundn(py_new',-6),roundn(pz_new',-6),c',b',roundn(a',-6),'VariableNames',{'x','y','z','c','b','a'}); 
writetable(ta, 'traj_plan_screw.csv');
 
traj_plot(px_new,py_new,pz_new,a,b,c,tn_new,VPx,VPy,VPz,tn,0);% 0=静态作图 1=动态作图


function J=costfuncxy(dt,px,py,pz)
kT=0;%时间优化系数
N=length(dt);
tnew=zeros(1,N+1);
for n=1:N
tnew(n+1)=tnew(n)+dt(n);
end
vx=(px(2:end)-px(1:end-1))./(tnew(2:end)-tnew(1:end-1));
ax=(vx(2:end)-vx(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
vy=(py(2:end)-py(1:end-1))./(tnew(2:end)-tnew(1:end-1));
ay=(vy(2:end)-vy(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
vz=(pz(2:end)-pz(1:end-1))./(tnew(2:end)-tnew(1:end-1));
az=(vz(2:end)-vz(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
J=sum(((ax.^2+ay.^2+az.^2)))+kT*sum(dt);
end


function [vx,vy,vz,ax,ay,az]=myplot(dt,px,py,pz)
N=length(dt);
tnew=zeros(1,N+1);
for n=1:N
tnew(n+1)=tnew(n)+dt(n);
end
vx=(px(2:end)-px(1:end-1))./(tnew(2:end)-tnew(1:end-1));
ax=(vx(2:end)-vx(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
vy=(py(2:end)-py(1:end-1))./(tnew(2:end)-tnew(1:end-1));
ay=(vy(2:end)-vy(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
vz=(pz(2:end)-pz(1:end-1))./(tnew(2:end)-tnew(1:end-1));
az=(vz(2:end)-vz(1:end-1))./(tnew(2:end-1)-tnew(1:end-2));
end