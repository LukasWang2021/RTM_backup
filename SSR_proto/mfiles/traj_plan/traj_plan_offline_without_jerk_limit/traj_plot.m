%traj_plot(x,y,z,a,b,c,Tnew,tn_new,VPx,VPy,VPz,tn,dynamic_flag) 可以给 xyz
%或者xyzabc 或者Tnew
%dynamic_flag是否动态作图 xyzabc是规划完的新角度，VP是原本的离散输入点 tn原输入采样时间 
function traj_plot(x,y,z,a,b,c,Tnew,tn_new,VPx,VPy,VPz,tn,dynamic_flag)
try
robot_sdh=evalin('base','robot_sdh');
catch
addpath(genpath('D:\实时侠同步\RTM_data\mytest_SSR'))
run('rtm_startup.m')
end
if nargin==7
    plot_flag=1 %用T作图
    tn_new=y;
    VPx=z;
    VPy=a;
    VPz=b;
    tn=c;
    dynamic_flag=Tnew;
    Tnew=x;
    Tin=Tnew;
    x=Tin(1,4,:); x=reshape(x,length(x),1);
    y=Tin(2,4,:); y=reshape(y,length(y),1);
    z=Tin(3,4,:); z=reshape(z,length(z),1);
    a=[];b=[];c=[];
elseif nargin==9
    plot_flag=3 %3维度
    tn=Tnew;
    dynamic_flag=VPx;
    tn_new=a;
    VPx=b;
    VPy=c;
    VPz=Tnew;
    a=[];
    b=[];
    c=[];
    Tnew=[];
elseif nargin==12
    plot_flag=6 %6维度
    dynamic_flag=tn;
    tn=VPz;
    VPz=VPy;
    VPy=VPx;
    VPx=tn_new;
    tn_new=Tnew;
    Tnew=[];
end
disp(['plot_flag=' num2str(plot_flag)])

%作图
traj_fig=figure('Name','Robot trajectory');set(traj_fig, 'position', get(0,'ScreenSize'));
plotro=subplot(6,2,[1 3 5 7 9 11]);hold on;grid on;axis([0 1 0 1 0 1]);
plotw1=subplot(6,2,2);hold on;grid on;title('6关节角加速度')
plotw2=subplot(6,2,4);hold on;grid on
plotw3=subplot(6,2,6);hold on;grid on
plotw4=subplot(6,2,8);hold on;grid on
plotw5=subplot(6,2,10);hold on;grid on
plotw6=subplot(6,2,12);hold on;grid on
plot3(plotro,x,y,z,'r','linewidth',1.2);
plot3(plotro,VPx,VPy,VPz,'k.','Markersize',0.5);
axis(plotro,[min(x) max(x)+0.1 min(y) max(y)+0.1 min(z) max(z)+0.1])
N_new=length(tn_new);
Qt=zeros(N_new,6);
Wt=zeros(N_new,6);
At=zeros(N_new,6);%角加速度
initial_pos=[0 0 0 0 -pi/2 0];%初始化机械臂位姿
init_T=rtm_fkine(robot_sdh,initial_pos);

% writerObj=VideoWriter('test.avi');
% open(writerObj)
 for n=1:N_new
% plot3(plotro,px_new(n),py(n),pz(n),'bx');hold on
if plot_flag==1
T=Tin(:,:,n);
elseif plot_flag==3
T=[init_T(1,1:3)     x(n);
   init_T(2,1:3)     y(n);
   init_T(3,1:3)     z(n);
   0     0     0     1];
elseif plot_flag==6
T=transl([x(n),y(n),z(n)])*trotz(10*c(n))*troty(10*b(n))*trotx(10*a(n));%角度测试
end
Qt(n,:)=rtm_ikine(robot_sdh,T,1,1,1,0);
if n>1
Wt(n,:)=(Qt(n,:)-Qt(n-1,:))/(tn_new(n)-tn_new(n-1));
end
if n>2
At(n,:)=(Wt(n,:)-Wt(n-1,:))/(tn_new(n)-tn_new(n-1));
end
    if dynamic_flag==1 %动态作图
        axes(plotro);
        robot_sdh.plot(Qt(n,:),'noshadow','jointdiam',0.1,'delay',0,'fps',60);
        plot(plotw1,tn_new(n),At(n,1),'r*');
        plot(plotw2,tn_new(n),At(n,2),'r*');
        plot(plotw3,tn_new(n),At(n,3),'r*');
        plot(plotw4,tn_new(n),At(n,4),'r*');
        plot(plotw5,tn_new(n),At(n,5),'r*');
        plot(plotw6,tn_new(n),At(n,6),'r*');

        pause(txyz(n+1)-txyz(n))
        % F=getframe(traj_fig);
        % writeVideo(writerObj,F);
    end
end
% close(writerObj);

if dynamic_flag==0 %静态作图
    if n==N_new(end)
        for j=1:2
            [~,ind1]=max(At);
            [~,ind2]=min(At);
            for i =1:6
                At(ind1(i),i)=0;
                At(ind2(i),i)=0;
            end
        end
    end
plot(plotw1,tn_new,At(:,1),'r*');
plot(plotw2,tn_new,At(:,2),'r*');
plot(plotw3,tn_new,At(:,3),'r*');
plot(plotw4,tn_new,At(:,4),'r*');
plot(plotw5,tn_new,At(:,5),'r*');
plot(plotw6,tn_new,At(:,6),'r*');
end

end

