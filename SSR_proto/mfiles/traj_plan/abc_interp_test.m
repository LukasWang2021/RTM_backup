q2 =[0.8138    0.4069    0.4069    0.0814];
q1 =[0.8660    0.2887    0.2887    0.2887];
q0 =[0.8138    0.4069    0.0814    0.4069];
sv0=quat2axang(q0);
sv1=quat2axang(q1);
sv2=quat2axang(q2);
%进行Slerp插值
SL1=rtm_SlerpN(q0,q1,10);
SL2=rtm_SlerpN(q1,q2,10);
for i=1:length(SL1)
sv_int1(i,:)=quat2axang(SL1(i,:));
end
for i=1:length(SL2)
sv_int2(i,:)=quat2axang(SL2(i,:));
end
figure 
sphere; hold on
plot3(sv_int1(:,1),sv_int1(:,2),sv_int1(:,3),'r')
plot3(sv_int2(:,1),sv_int2(:,2),sv_int2(:,3),'r')
plot3(sv0(1),sv0(2),sv0(3),'rx');
plot3(sv1(1),sv1(2),sv1(3),'gx');
plot3(sv2(1),sv2(2),sv2(3),'bx');

quat=quaternion([q0;q1;q2]);
quatavg = meanrot(quat);
sv_avg=quat2axang(quatavg);
plot3(sv_avg(1),sv_avg(2),sv_avg(3),'ro');

R0=rtm_angvec2r(q0);
R1=rtm_angvec2r(q1);
R2=rtm_angvec2r(q2);
ez0=[0 0 1]';
r0=R0*ez0;
r1=R1*ez0;
r2=R2*ez0;
Rm=quat2rotm(quatavg);
rm=Rm*ez0;
figure
plot3(r0(1),r0(2),r0(3),'ro');hold on
plot3(r1(1),r1(2),r1(3),'ro')
plot3(r2(1),r2(2),r2(3),'ro')
plot3(rm(1),rm(2),rm(3),'bo')
sphere
xlabel('x')
title('四元数三点均值z轴朝向')
%% test realtraj
[Ponline,Qonline,T]=traj_generate('out4.txt');
ez0=[0 0 1]';
N=length(Qonline);
R=zeros(3,3,N);
r=zeros(N,3);
for i = 1:N
   R(:,:,i)=rtm_angvec2r(Qonline(i,:));
   r(i,:)=(R(:,:,i)*ez0)';   
end
a=200;
b=400;%取部分段作图

Qs=smooth_quat(Qonline,7);%smooth quaternion
Rs=zeros(3,3,N);
rs=zeros(N,3);
for i = 1:N
   Rs(:,:,i)=rtm_angvec2r(Qs(i,:));
   rs(i,:)=(Rs(:,:,i)*ez0)';   
end

figure
sphere
hold on
plot3(r(a:b,1),r(a:b,2),r(a:b,3),'r'); hold on
for n=a:b
plot3(r(n,1),r(n,2),r(n,3),'rx'); hold on
% plot3(rs(n,1),rs(n,2),rs(n,3),'bo')
% pause(0.05)
end

figure
plot3(r(a:b,1),r(a:b,2),r(a:b,3),'r'); hold on
plot3(rs(a:b,1),rs(a:b,2),rs(a:b,3),'b')
xlabel('x')
ylabel('y')
grid on
title('平滑前后z轴朝向轨迹')