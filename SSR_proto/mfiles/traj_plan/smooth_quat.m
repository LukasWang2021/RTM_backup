%Qs=smooth_quat(Q,N) 均值平滑四元数，输入四元数行向量q组成的矩阵和平滑窗口宽度N,N必须为奇数
function Qs=smooth_quat(Q,N)
if mod(N,2)==0
    error('平滑窗口宽度N必须为奇数')
end
for i = 1:length(Q)
    if i<=(N-1)/2 || i>=length(Q)-(N-1)/2
        Qs(i,:)=Q(i,:);
%         Qs(i,:)=Qs(i,:)/norm(Qs(i,:));
    else
        for k=1:N
            quats(k,:)=quaternion(Q(i+(k-1-(N-1)/2),:));%取i前后N个四元数
            quatavg = meanrot(quats);%曲均值
            [s,v1,v2,v3]=quatavg.parts;
            Qs(i,:)=[s,v1,v2,v3];
%             Qs(i,:)=Qs(i,:)/norm(Qs(i,:));
        end
    end
end

end