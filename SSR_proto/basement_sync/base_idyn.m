%% Step1&2
%%test without friction and motor side parameters
L1 = Prismatic( 'a', 0, 'alpha', pi/2, 'offset',0, 'm', 5,'qlim',[0 1],'r',[0 0 -0.1],'G',1);
L2 = Prismatic( 'a', 0.4, 'alpha', 0,    'offset',0, 'm', 4,'qlim',[0 1],'r',[0 0 -0.2],'G',1);
L3 = Revolute('d',0,      'a',0.4,   'm', 13,'r',[0 0 0.1],'G',1);
robot_base = SerialLink([L1,L2,L3], 'name', 'Puma Base');
robot_base.base = [1 0 0 0;0 0 1 0 ;0 -1 0 0;0 0 0 1]; % base is the frame of the prismatic joint base


L=robot_base.links;
d1=L(1).d;d2=L(2).d;d3=L(3).d;
a1=L(1).a;a2=L(2).a;a3=L(3).a;
f1=L(1).alpha;f2=L(2).alpha;f3=L(3).alpha;
m1=L(1).m;m2=L(2).m;m3=L(3).m;
cog1=L(1).r;cog2=L(2).r;cog3=L(3).r;
I1=L(1).I;I2=L(2).I;I3=L(3).I;
Jm1=L(1).Jm;Jm2=L(2).Jm;Jm3=L(3).Jm;
G1=L(1).G;G2=L(2).G;G3=L(3).G;

%toolbox calculate
g=[0 0 -9.81]';
Fext=[1 1 0]';
Mext=[1 1 0]';
q=[0 0 pi/3];
dq = [0 0 0];
ddq =[0 0 0];
tau = robot_base.rne(q,dq,ddq,'fext',[Fext;Mext]')
robot_base.plot(q,'workspace',[-1 2 -1 2 -1 2],'jvec')
%% idyn model 
% syms q1 q2 q3 q4 q5 q6 dq1 dq2 dq3 dq4 dq5 dq6 ddq1 ddq2 ddq3 ddq4 ddq5 ddq6...
% rx1 ry1 rz1 rx2 ry2 rz2 rx3 ry3 rz3 rx4 ry4 rz4 rx5 ry5 rz5 rx6 ry6 rz6...   
% Ixx1 Iyy1 Izz1 Ixy1 Iyz1 Ixz1 Ixx2 Iyy2 Izz2 Ixy2 Iyz2 Ixz2 ...
% Ixx3 Iyy3 Izz3 Ixy3 Iyz3 Ixz3 Ixx4 Iyy4 Izz4 Ixy4 Iyz4 Ixz4...
% Ixx5 Iyy5 Izz5 Ixy5 Iyz5 Ixz5 Ixx6 Iyy6 Izz6 Ixy6 Iyz6 Ixz6...
% m1 m2 m3 m4 m5 m6...

% q=[q1 q2 q3 q4 q5 q6];
% dq=[dq1 dq2 dq3 dq4 dq5 dq6];
% ddq=[ddq1 ddq2 ddq3 ddq4 ddq5 ddq6];
% 
% dq1=dq(1);%use the same value as toolbox
% dq2=dq(2);
% dq3=dq(3);
% 
% ddq1=ddq(1);
% ddq2=ddq(2);
% ddq3=ddq(3);

% cog1=[rx1 ry1 rz1];
% cog2=[rx2 ry2 rz2];
% cog3=[rx3 ry3 rz3];

% I1=[Ixx1 Ixy1 Ixz1;Ixy1 Iyy1 Iyz1;Ixz1 Iyz1 Izz1];
% I2=[Ixx2 Ixy2 Ixz2;Ixy2 Iyy2 Iyz2;Ixz2 Iyz2 Izz2];
% I3=[Ixx3 Ixy3 Ixz3;Ixy3 Iyy3 Iyz3; Ixz3 Iyz3 Izz3];
tic
DH=[ L(1).offset   q(1)    L(1).a   L(1).alpha;
     L(2).offset   q(2)    L(2).a   L(2).alpha;   
    q(3)+ L(3).offset   L(3).d    L(3).a   L(3).alpha;];

Base =robot_base.base;  
Tbase= [ Base.n Base.o Base.a Base.t;
         0        0        0      1];

T01=[cos(DH(1,1))   -cos(DH(1,4))*sin(DH(1,1))     sin(DH(1,4))*sin(DH(1,1))   DH(1,3)*cos(DH(1,1));
     sin(DH(1,1))    cos(DH(1,4))*cos(DH(1,1))     -sin(DH(1,4))*cos(DH(1,1))   DH(1,3)*sin(DH(1,1));
     0               sin(DH(1,4))                   cos(DH(1,4))                DH(1,2);
     0               0                             0                            1];     
T12=[cos(DH(2,1))   -cos(DH(2,4))*sin(DH(2,1))      sin(DH(2,4))*sin(DH(2,1))   DH(2,3)*cos(DH(2,1));
     sin(DH(2,1))    cos(DH(2,4))*cos(DH(2,1))     -sin(DH(2,4))*cos(DH(2,1))   DH(2,3)*sin(DH(2,1));
     0               sin(DH(2,4))                   cos(DH(2,4))                DH(2,2);
     0               0                             0                            1];
T23=[cos(DH(3,1))   -cos(DH(3,4))*sin(DH(3,1))      sin(DH(3,4))*sin(DH(3,1))   DH(3,3)*cos(DH(3,1));
     sin(DH(3,1))    cos(DH(3,4))*cos(DH(3,1))     -sin(DH(3,4))*cos(DH(3,1))   DH(3,3)*sin(DH(3,1));
     0               sin(DH(3,4))                   cos(DH(3,4))                DH(3,2);
     0               0                             0                            1];

 
% T01=roundn(T01,-5);
% T12=roundn(T12,-5);
T01=Tbase*T01;
T02=T01*T12;
T03=T02*T23;

Rbase=Tbase(1:3,1:3);
R01=T01(1:3,1:3);
R02=T02(1:3,1:3);
R03=T03(1:3,1:3);

Fext=R03*Fext;
Mext=R03*Mext;

r0s1=T01*[cog1 1].';
r0s1=r0s1(1:3);
r0s2=T01*T12*[cog2 1].';
r0s2=r0s2(1:3);
r0s3=T02*T23*[cog3 1].';
r0s3=r0s3(1:3);

r01=T01(1:3,4); %0系到1系坐标距离
% r02=r01+R01*T12(1:3,4); %0系到2系
r02=T02(1:3,4);
r03=T03(1:3,4);
r12=r02-r01;
r23=r03-r02;
r1s1=r0s1-r01; %1系到1重心
r2s2=r0s2-r02;
r3s3=r0s3-r03;
r1s2=r0s2-r01;
r2s3=r0s3-r02;

ezbase=[0 0 1]';
ez0 = Rbase*ezbase;
ez1=R01*ezbase;
ez2=R02*ezbase;


W01=[0 0 0]';
W02=W01;
W03=W02+dq(3)*ez2;

dW01=[0 0 0]';
dW02=dW01;
dW03=dW02+ddq(3)*ez2+dq(3)*cross(W02,ez2);

%~~~~~~~~~~~~~~~待修改 
dr01=cross(W01,r01)+dq(1)*ez0;
dr02=dr01+cross(W02,r12)+dq(2)*ez1;
dr03=dr02+cross(W03,r23);

ddr01=ddq(1)*ez0+cross(W01,cross(W01,r01))+2*dq(1)*cross(W01,ez0)+cross(dW01,r01);
ddr02=ddr01+ddq(2)*ez1+cross(W02,cross(W02,r12))+2*dq(2)*cross(W02,ez1)+cross(dW02,r12);
ddr03=ddr02+cross(dW03,r23)+cross(W03,cross(W03,r23));

dr0s1=dr01+cross(W01,r1s1);
dr0s2=dr02+cross(W02,r2s2);
dr0s3=dr03+cross(W03,r3s3);

ddr0s1=ddr01+cross(dW01,r1s1)+cross(W01,cross(W01,r1s1));
ddr0s2=ddr02+cross(dW02,r2s2)+cross(W02,cross(W02,r2s2));
ddr0s3=ddr03+cross(dW03,r3s3)+cross(W03,cross(W03,r3s3));

J01=R01*I1*R01';
J02=R02*I2*R02';
J03=R03*I3*R03';

F23=m3*ddr0s3+Fext-m3*g; 
F12=m2*ddr0s2+F23-m2*g; 
F01=m1*ddr0s1+F12-m1*g;%F02

M23=J03*dW03+cross(W03,J03*W03)+Mext-cross(r3s3,Fext)+cross(r2s3,F23);
M12=J02*dW02+cross(W02,J02*W02)+M23-cross(r2s2,F23)+cross(r1s2,F12);%M02=
M01=J01*dW01+cross(W01,J01*W01)+M12-cross(r1s1,F12)+cross(r0s1,F01);

tau1=F01.'*ez0 ;%+ Jm1*G1^2*ddq(1);
tau2=F12.'*ez1 ;%+ Jm2*G2^2*ddq(2);
tau3=M23.'*ez2 + Jm3*G3^2*ddq(3);
taus=[tau1 tau2 tau3];

toc
% [co_para,t_para]=coeffs(tau1,[dq1 dq2 dq3 dq4 dq5 dq6 ddq1 ddq2 ddq3 ddq4 ddq5 ddq6]); 


% r_s3_0
% r_s4_0
% r_s5_0
% r_s6_0

