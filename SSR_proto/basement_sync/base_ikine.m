%% inverse kinematic for the basement synchronizer,base on end pos Pe=[x y z]

function q = base_ikine(robot,Pe)
x=Pe(1);
y=Pe(2);
z=Pe(3);
L=robot.links(3).a;
q1=y+sqrt(L^2-x^2);
q2=z;
q3=atan(x/(q1-y))-pi/2;
q=[q1 q2 q3];

