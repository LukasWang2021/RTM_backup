%caculate X and Y of circle
function [sys, x0, str, ts] = base_init_traj(t, x, u, flag,robot,px,py,pz)
	switch flag

	case 0
		% initialize the robot graphics
		[sys,x0,str,ts] = mdlInitializeSizes;	% Init

	case {3}
	    sys=mdlOutput(t,x,u,robot,px,py,pz);
        
	case {1, 2, 4, 9}
		sys = [];
	end

%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes
 
%
% call simsizes for a sizes structure, fill it in and convert it to a
% sizes array.
%
% Note that in this example, the values are hard coded.  This is not a
% recommended practice as the characteristics of the block are typically
% defined by the S-function parameters.
%
sizes = simsizes;
 
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 3;
sizes.NumInputs      = 0;
sizes.DirFeedthrough = 0;
sizes.NumSampleTimes = 1;   % at least one sample time is needed

sys = simsizes(sizes);

%
% initialize the initial conditions
%
x0  = [];
 
%
% str is always an empty matrix
%
str = [];
 
%
% initialize the array of sample times
%
ts  = [-1 0];
 
% end mdlInitializeSizes
function sys=mdlOutput(t,x,u,robot,px,py,pz)
te = 5; %多久到达指定位置
if t<=te
L = robot.links(3).a;
vx = (px-L)/te;
vy = py/te;
vz = pz/te;
sys=[L+vx*t vy*t vz*t];
end
if t>te
sys = [px py pz];    
end
