cd('D:\实时侠同步\RTM_data')
addpath(genpath('mytest_SSR'))
L1 = Prismatic( 'a', 0,    'alpha', pi/2, 'offset',0, 'm', 5,'qlim',[0 1],'r',[0 0 -0.1]);
L2 = Prismatic( 'a', 0, 'alpha', 0,    'offset',0, 'm', 4,'qlim',[0 1],'r',[0 0 -0.2]);
L3 = Revolute('d',0,      'a',0.4,   'm', 3,'r',[0 0 -0.1]);
robot_base = SerialLink([L1,L2,L3], 'name', 'Puma Base');
robot_base.base = [1 0 0 0;0 0 1 0 ;0 -1 0 0;0 0 0 1];
robot_base.plot([0 0 0],'workspace',[-1 1 -1 1 -1 1],'jvec')

%test ikine
% for i = [1:10]
% q_give = [0.1*i 0.2 -0.05*pi*i];
% Pe = base_fkine(robot_base,q_give)
% q_cal = base_ikine(robot_base,Pe)
% robot_base.plot(q_cal,'workspace',[-1 1 -1 1 -1 1],'jvec')
% end


%%admittance parameters
mv = 0.1;
bv = 0.1;
kv = 100;