function Pe = base_fkine(robot,q)
L=robot.links(3).a;
x = L*cos(q(3));
y = q(1)+L*sin(q(3));
z = q(2);
Pe = [x y z];