close all;clear all;clc;
%% 产生测试信号
fs = 1000;
f0 = 10; %50 Hz
f1 = 20;
f2 = 40;
f3 = 100;
n = 1000;
t = (0:n-1)'/fs;
xs = sin(2*pi*f2*t)+sin(2*pi*f1*t)+cos(2*pi*f0*t);
xs = xs+0.5*rand(size(xs,1),1)+0.2*cos(2*pi*f3*t);
ws=V./16777216;
xs=V_feed./16777216; plot(xs);hold on;plot(ws)
% figure;
% subplot(211);plot(t, xs);title('原始信号');
% subplot(212);plot(t, ws);title('加噪信号');

M  = 5;   % 滤波器的阶数
xn = ws;
dn = xs;
% rho_max = max(eig(ws(5000:6000)*ws(5000:6000).'));   % 输入信号相关矩阵的最大特征值
% mu = (1/rho_max)/2 ;    % 收敛因子 0 < mu < 1/rho
mu = 1;
[yn,W,en] = lmsFunc(xn,dn,M,mu);

% figure;
% ax1 = subplot(211);
% plot(t,ws);grid on;ylabel('幅值');xlabel('时间');
% ylim([-1.5 1.5]);title('LMS滤波器输入信号');
% 
% ax2 = subplot(212);
% plot(t,yn);grid on;ylabel('幅值');xlabel('时间');title('LMS滤波器输出信号');
% ylim([-1.5 1.5]);linkaxes([ax1, ax2],'xy');

figure;plot(en);grid on;title('误差');

figure;plot(dn);grid on;hold on;plot(ws);plot(yn);
legend('期望信号','实际信号','滤波后信号')


%%  输入参数:
%   xn   输入的信号
%   dn   所期望的响应
%   M    滤波器的阶数
%   mu   收敛因子(步长)
% 输出参数:
%   W    滤波器系数矩阵  
%   en   误差序列 
%   yn   滤波器输出        
function [yn, W, en]=lmsFunc(xn, dn, M, mu)
itr = length(xn);
en = zeros(itr,1);            
W  = zeros(M,itr);    % 每一列代表-次迭代,初始为0
% 迭代计算
for k = M:itr                  % 第k次迭代
    x = xn(k-M+1:1:k);        % 滤波器M个抽头的输入
    y = W(:,k-1).' * x;        % 滤波器的输出
    en(k) = dn(k) - y ;        % 第k次迭代的误差
    % 滤波器权值计算的迭代式
    W(:,k) = W(:,k-1) + 2*mu*en(k)*x;
end

yn = inf * ones(size(xn)); % 初值为无穷大是绘图使用，无穷大处不会绘图
for k = M:length(xn)
    x = xn(k:-1:k-M+1);
    yn(k) = W(:,end).'* x;  % 最终输出结果
end
end
