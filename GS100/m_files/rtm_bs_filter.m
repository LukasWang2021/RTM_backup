%给定采样频率，带阻频率 获得Z域的离散滤波器 num den系数
function [n_cal,d_cal]=rtm_bs_filter(fs,fs1,fs2)
ws1=2*pi*fs1/fs;%带阻频率1 下降沿 数字量参数
ws2=2*pi*fs2/fs;

num_coeff= [5000*cos(ws1 - ws2) + 5000, - 20000*cos(ws1) - 20000*cos(ws2), 10000*cos(ws1 - ws2) + 20000*cos(ws1 + ws2) + 30000, - 20000*cos(ws1) - 20000*cos(ws2), 5000*cos(ws1 - ws2) + 5000];
num_coeff=vpa(num_coeff,4);

n_cal = double(num_coeff)

den_coeff= [10000 - 7071*sin(ws1 - ws2), 14142*sin(ws1) - 20000*cos(ws2) - 20000*cos(ws1) - 14142*sin(ws2), 20000*cos(ws1 - ws2) + 20000*cos(ws1 + ws2) + 20000, 14142*sin(ws2) - 20000*cos(ws2) - 14142*sin(ws1) - 20000*cos(ws1), 7071*sin(ws1 - ws2) + 10000];
den_coeff=vpa(den_coeff,4);
d_cal=double(den_coeff)

[h,w]=freqz(n_cal,d_cal,round(fs/2),fs);
plot(w,abs(h));grid on;

end