%%low pass filter -3db at fp
syms s Z z fs fp
fs=5000;
Ts=1/fs;
fp=100; 
wp=2*pi*fp/fs;%归一化
% delp=0.3;%通带最大衰减 20log(1-delp) 即衰减幅度
% dels=0.1;%阻带最大幅值 

%% 阶数选择
% d=(((1-delp)^(-2)-1)/(dels^(-2)-1))^0.5;
% k=fp/fst;
% N=ceil(log(d)/log(k)) %order selection
% fc = (fp*((1-delp)^(-2)-1)^(-1/2/N)) + fst*(dels^(-2)-1)^(-1/2/N))/2;%3db截至频率即可选为通带和阻带的中心频率
% wc=fc/fs*2*pi;%离散系临界频率

%%
wc=wp;%此处取默认临界点为通带截止频率
Omegac=tan(wc/2);%连续S域临界频率
% F(s) = 1/(s^6+3.8637*s^5+7.4641*s^4+9.1416*s^3+7.4641*s^2+3.8637*s+1) %截止频率归一化后通用公式 相当于buttap(6)
F(s) = 1/(s^2+1.4142*s+1); %二阶低通
s=s/Omegac; %反归一化
F=subs(F);
s=(1-z^-1)/(1+z^-1);
F(z)=subs(F(s));

%simplify tf
F_e=collect(F);
F_e=simplify(F_e);
[num,den]=numden(F_e);

num=collect(num);
den=collect(den);
n=sym2poly(num);
d=sym2poly(den);
[h,w]=freqz(n,d,2560,fs);
figure
plot(w,abs(h));grid on;
figure
freqz(n,d,2560,fs);

%% generate gernal function
syms Z fp fs
F_e(z) = -(5000*(cos((2*pi*fp)/fs) - 1)*(z + 1)^2)/(7071*z^2*sin((2*pi*fp)/fs) - 20000*z*cos((2*pi*fp)/fs) - 7071*sin((2*pi*fp)/fs) + 10000*z^2 + 10000);

num=-5000*(cos((2*pi*fp)/fs) - 1)*(z + 1)^2;
den=7071*z^2*sin((2*pi*fp)/fs) - 20000*z*cos((2*pi*fp)/fs) - 7071*sin((2*pi*fp)/fs) + 10000*z^2 + 10000;

num=collect(num);
den=collect(den);
[n,~]=coeffs(num,z);%[5000 - 5000*cos((2*pi*fp)/fs), 10000 - 10000*cos((2*pi*fp)/fs), 5000 - 5000*cos((2*pi*fp)/fs)]
[d,~]=coeffs(den,z);%[10000 - 7071*sin((2*pi*fp)/fs), -20000*cos((2*pi*fp)/fs), 7071*sin((2*pi*fp)/fs) + 10000]


[h,w]=freqz(n_cal,d_cal,2560,fs);
plot(w,abs(h));grid on;
