%%low pass filter --> S to Z with bilinear method --> band shift -->band stop filter 
syms s Z z fs fp fst fs1 fs2 ws1 ws2 delp dels 
% fs=5000;
Ts=1/fs;
fp=100;
fst=600;
% fs1=500;
% fs2=800;
wp=2*pi*fp/fs;%归一化
wst=2*pi*fst/fs;
% delp=0.3;%通带最大衰减 20log(1-delp) 即衰减幅度
% dels=0.1;%阻带最大幅值 

ws1=2*pi*fs1/fs;%带阻频率1 下降沿 数字量参数
ws2=2*pi*fs2/fs;
% Omegas1=tan(ws1/2)%模拟量参数
% Omegas2=tan(ws2/2)

%% 阶数选择
% d=(((1-delp)^(-2)-1)/(dels^(-2)-1))^0.5;
% k=fp/fst;
% N=ceil(log(d)/log(k)) %order selection
% fc = (fp*((1-delp)^(-2)-1)^(-1/2/N)) + fst*(dels^(-2)-1)^(-1/2/N))/2;%3db截至频率即可选为通带和阻带的中心频率
% wc=fc/fs*2*pi;%离散系临界频率

%%
wc=wp;%此处取默认临界点为通带截止频率
Omegac=tan(wc/2);%连续S域临界频率
% F(s) = 1/(s^6+3.8637*s^5+7.4641*s^4+9.1416*s^3+7.4641*s^2+3.8637*s+1) %截止频率归一化后通用公式 相当于buttap(n)
F(s) = 1/(s^2+1.4142*s+1)
s=s/Omegac; %反归一化
F=subs(F)
s=(1-z^-1)/(1+z^-1);
F(z)=subs(F(s))
%shift band
d1=(-2*cos((ws1+ws2)/2)/cos((ws2-ws1)/2))/(1+tan(wc/2)*tan((ws2-ws1)/2));
d2=(1-tan(wc/2)*tan((ws2-ws1)/2))/(1+tan(wc/2)*tan((ws2-ws1)/2));
z=((Z^-2 + d1*Z^-1 + d2)/(d2*Z^-2 + d1*Z^-1 + 1))^-1;
F(Z)=subs(F(z))
%simplify tf
F_e=collect(F)
F_e=simplify(F_e);
 [num,den]=numden(F_e)
  n=sym2poly(num);
  d=sym2poly(den);
  n=n./d(1)
  d=d./d(1)
%   [nz,dz]=bilinear(n,d,0.5)
[h,w]=freqz(n,d,2560,fs);
plot(w,abs(h));grid on;


%% generate gernal function
syms Z
% num(Z) = 5000*cos(ws1 - ws2) + 10000*Z^2*cos(ws1 - ws2) + 5000*Z^4*cos(ws1 - ws2) - 20000*Z*cos(ws1) - 20000*Z*cos(ws2) + 30000*Z^2 + 5000*Z^4 + 20000*Z^2*cos(ws1 + ws2) - 20000*Z^3*cos(ws1) - 20000*Z^3*cos(ws2) + 5000;
% num = collect(num);
% [num_coeff,Z_order]=coeffs(num,Z)

num_coeff= [5000*cos(ws1 - ws2) + 5000, - 20000*cos(ws1) - 20000*cos(ws2), 10000*cos(ws1 - ws2) + 20000*cos(ws1 + ws2) + 30000, - 20000*cos(ws1) - 20000*cos(ws2), 5000*cos(ws1 - ws2) + 5000];
num_coeff=vpa(num_coeff,4);

n_cal = double(num_coeff)
% Z_order(Z) =
% [Z^4, Z^3, Z^2, Z, 1]
 
% den(Z) = 7071*sin(ws1 - ws2) + 14142*Z^3*sin(ws1) - 14142*Z^3*sin(ws2) + 20000*Z^2*cos(ws1 - ws2) - 7071*Z^4*sin(ws1 - ws2) - 20000*Z*cos(ws1) - 20000*Z*cos(ws2) - 14142*Z*sin(ws1) + 14142*Z*sin(ws2) + 20000*Z^2 + 10000*Z^4 + 20000*Z^2*cos(ws1 + ws2) - 20000*Z^3*cos(ws1) - 20000*Z^3*cos(ws2) + 10000;
% den = collect(den);
% [den_coeff,Z_order]=coeffs(den,Z);

den_coeff= [10000 - 7071*sin(ws1 - ws2), 14142*sin(ws1) - 20000*cos(ws2) - 20000*cos(ws1) - 14142*sin(ws2), 20000*cos(ws1 - ws2) + 20000*cos(ws1 + ws2) + 20000, 14142*sin(ws2) - 20000*cos(ws2) - 14142*sin(ws1) - 20000*cos(ws1), 7071*sin(ws1 - ws2) + 10000];
den_coeff=vpa(den_coeff,4);
d_cal=double(den_coeff)

 

[h,w]=freqz(n_cal,d_cal,2560,fs);
plot(w,abs(h));grid on;
