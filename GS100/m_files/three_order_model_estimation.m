cd('D:\实时侠同步\RTM_data')
addpath(genpath('D:\实时侠同步\RTM_data\GS100\gs100\DEMO测试数据\PPX'))
data=csvread('2021_8_25_16_39_39.csv',1,0);
v_give=data(:,1)/16777216;
v_feed=data(:,2)/16777216;
p_give=data(:,3)/16777216;
p_feed=data(:,4)/16777216;
I_give=data(:,5)/16777216;
I_feed=data(:,6)/16777216;
inputs=I_feed;
out_target=p_feed;

%% generate targets
Ts=200e-6;
t=0:Ts:0.5;
wn=2*pi/0.01;
u=sin(wn*t);
% u(1:0.001/Ts)=1;%脉冲0.001s
% u(0.001/Ts+1:0.5/Ts+1)=0;
Tm=0.0190;
TL=0.0127;
Bm=0.1078;
BL=0;
Tc=1.5215e-02;
co_num = [TL*Tc, BL*Tc, 1];
co_den = [TL*Tc*Tm, BL*Tc*Tm + Bm*TL*Tc, TL + Tm + BL*Bm*Tc, BL + Bm];
out_target=lsim(tf(co_num,co_den),u,t);

%%拟合
f = @(paras)cost_func(paras,u,t,out_target);%parameters=[Tm TL Bm BL Tc]
lb = [0.01,0.005,0.05,0,0]; %上下界
ub = [0.1,0.1,0.5,0.01,1e-5];
x0=[0.02,0.01,0.1,0,1e-6];
options = optimoptions(@lsqnonlin,'Algorithm','trust-region-reflective','FunctionTolerance',1e-7,'MaxFunctionEvaluations',1000*5);
x = lsqnonlin(f,x0,lb,ub,options)
xd=[Tm TL Bm BL Tc]
Tm=x(1);
TL=x(2);
Bm=x(3);
BL=x(4);
Tc=x(5);
co_num = [TL*Tc, BL*Tc, 1];
co_den = [TL*Tc*Tm, BL*Tc*Tm + Bm*TL*Tc, TL + Tm + BL*Bm*Tc, BL + Bm];
out_sim=lsim(tf(co_num,co_den),u,t);
J=var(out_sim-out_target)
plot(t,out_sim,t,out_target)

%% 
rng default
% opts = optimoptions(@fmincon,'Algorithm','sqp')
% ms = MultiStart('FunctionTolerance',1e-6,'UseParallel',true)
% gs = GlobalSearch(ms);
% problem = createOptimProblem('fmincon','x0',x0,...
%     'objective',f,'lb',lb,'ub',ub,'options',opts);
% [x,J] = run(gs,problem)
opts=optimoptions(@ga,'FunctionTolerance',1e-6);
[x,J] = ga(f,5,[],[],[],[],lb,ub,[],opts);
Tm=x(1);
TL=x(2);
Bm=x(3);
BL=x(4);
Tc=x(5);
co_num = [TL*Tc, BL*Tc, 1];
co_den = [TL*Tc*Tm, BL*Tc*Tm + Bm*TL*Tc, TL + Tm + BL*Bm*Tc, BL + Bm];
out_sim=lsim(tf(co_num,co_den),u,t);
J=var(out_sim-out_target)
plot(t,out_sim,t,out_target)

function J=cost_func(paras,u,t,out_target)
Tm=paras(1);
TL=paras(2);
Bm=paras(3);
BL=paras(4);
Tc=paras(5);
% A=[-Bm/Tm    0    -1/Tm    0;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
%      0      -BL/TL    1/TL   -1/TL;
%    1/Tc   -1/Tc   0       0;
%      0      0     0       0];
% B=[1/(Tm);0;0;0];
% C=[1 0 0 0];
% D=0;
% Gs=C*inv(s*eye(4)-A)*B;
% [nums,dens]=numden(Gs);
% [co_num,num_ts]=coeffs(nums,s,'all');
% [co_den,den_ts]=coeffs(dens,s,'all');
% co_num=eval(co_num);
% co_den=eval(co_den);
% co_num=co_num/co_den(1);
% co_den=co_den/co_den(1);
co_num = [TL*Tc, BL*Tc, 1];
co_den = [TL*Tc*Tm, BL*Tc*Tm + Bm*TL*Tc, TL + Tm + BL*Bm*Tc, BL + Bm];
Gs=tf(co_num,co_den);
Gd=c2d(Gs,200e-6);
out_sim=lsim(Gd,u,t);
J=var(out_sim-out_target);
end