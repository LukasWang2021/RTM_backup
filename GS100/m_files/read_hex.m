%%read csv hex
data = importdata('iladata(3)(1).csv');
Iq = data.textdata(6500:7500,7);
Iq = hex2dec(Iq);
digit = 32;
for i = 1:length(Iq)
    if Iq(i) > 2^digit/2-1
        Iq(i) = Iq(i) - 2^digit;
    end
end
Iq_32 = int32(Iq);

a = figure('name','origin');
Fs = 5000; %sampling 200ms
L=length(Iq_32);
Iq_ff = fft(Iq_32);

P2 = abs(Iq_ff/L);
P1 = P2(1:L/2);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L-1))'/L;
plot(f(1:size(P1)),P1)