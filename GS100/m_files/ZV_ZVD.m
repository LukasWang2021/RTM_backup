%输入整形器 ZV vs ZVD
%% get ksi and wn through experiment
t0=0.0299687;
t1=0.040937;
Ap=0.0100868-0.0099;
An=0.0099-0.009899;
phi=(Ap)/(An);%相邻波峰波谷绝对值比值
Td=2*(t1-t0);
delta=log(phi);
ksi=delta/sqrt(pi^2+delta^2)
wd=2*pi/Td
wn=wd/sqrt(1-ksi^2) %measured system wn

% ksi=0.8;
% wn=200;%measured system
wn1=1*wn; %real system wn
Ts=0.0001;
wd=wn*sqrt(1-ksi^2);
GS=ss(tf(wn1^2,[1 2*ksi*wn1 wn1^2]));
step(GS)
% GSS=ss(tf([wn1^2 0],[1 2*ksi*wn1 wn1^2]));%速度函数 同样可以计算出固有频率和ksi
% GS=c2d(GS,Ts);

%% ZV
t1=0;
t2=pi/wd;
A1=exp(ksi*wn*t2)/(1+exp(ksi*wn*t2));%ZV 幅值
A2=1/(1+exp(ksi*wn*t2));


FS=tf(A1,'inputdelay',t1)+tf(A2,'inputdelay',t2);
% FS=c2d(FS,Ts);
FG_ZV=series(FS,GS);

% figure('Name','step response')
% step(GS);hold on
% step(FG_ZV)
% 
% figure('name','trapezoid response')
% t=0:Ts:0.2;
% u(1:0.1/Ts)=10*t(1:0.1/Ts);
% u(0.1/Ts+1:0.2/Ts+1)=1;
% lsim(GS,u,t);hold on
% lsim(FG_ZV,u,t)

figure('name','sinewave response')
t=0:Ts:0.5;
Tu=0.1;
% u=sin(t/Tu*2*pi);
u(1:0.001/Ts)=1;%脉冲0.001s
u(0.001/Ts+1:0.5/Ts+1)=0;
lsim(GS,u,t);hold on
lsim(FG_ZV,u,t)
% figure
% lsim(GSS,u,t);hold on



%% ZVD
t1=0;
t2=pi/wd;
t3=2*pi/wd;
A1=1/(1+2*exp(-ksi*wn*t2)+exp(-ksi*wn*t3));
A2=2*exp(-ksi*wn*t2)/(1+2*exp(-ksi*wn*t2)+exp(-ksi*wn*t3));
A3=exp(-ksi*wn*t3)/(1+2*exp(-ksi*wn*t2)+exp(-ksi*wn*t3));


FS=tf(A1,'inputdelay',t1)+tf(A2,'inputdelay',t2)+tf(A3,'inputdelay',t3);
% FS=c2d(FS,Ts);
FG_ZVD=series(FS,GS);

% figure('Name','step response')
% step(GS);hold on
% step(FG_ZVD)
% 
% figure('name','trapezoid response')
% t=0:Ts:0.2;
% u(1:0.1/Ts)=10*t(1:0.1/Ts);
% u(0.1/Ts+1:0.2/Ts+1)=1;
% lsim(GS,u,t);hold on
% lsim(FG_ZVD,u,t)
% 
% figure('name','sinewave response')
% t=0:Ts:0.2;
% Tu=0.1;
% u=sin(t/Tu*2*pi);
% u(0.1/Ts+1:0.2/Ts+1)=1;
% lsim(GS,u,t);hold on
lsim(FG_ZVD,u,t)

