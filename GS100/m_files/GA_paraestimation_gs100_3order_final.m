%% test for identify the parameters of ripple force model
%如果修改模型函数，需要修改mymodel输入变量和调用的接口以及函数本体，还有初始化的dim,bound 即有**备注的地方
clear
Tm=0.0190;
TL=0.0127;
Bm=0.1078;
BL=0;
Tc=1.5215e-07;
A1=Tm;%模型待识别参数真实值
A2=TL;
A3=Bm;
A4=BL;
A5=Tc;

Ts=200e-6;
% t=0:Ts:0.2;
% nsteps=100;
% for ti=1:length(t)
%     k=round(ti/nsteps);
%     if mod(k,2)==0
%     u(ti)=0;
%     else
%     u(ti)=1;
%     end
% end
t=0:Ts:0.2-Ts;
t=0:Ts:1-Ts;
% u(1:0.01/Ts)=0;
% u(0.01/Ts+1:0.02/Ts)=1;
% u(0.02/Ts:0.2/Ts)=0;
wn=2*pi/0.02;
u=0.1*sin(wn*t)+0.5*sin(10*wn*t)+1*sin(20*wn*t);
co_den =[BL + Bm + 10000.0*TL + 10000.0*Tm + 10000.0*BL*Bm*Tc + 100000000.0*BL*Tc*Tm + 100000000.0*Bm*TL*Tc + 1000000000000.0*TL*Tc*Tm, 3.0*BL + 3.0*Bm + 10000.0*TL + 10000.0*Tm + 10000.0*BL*Bm*Tc - 100000000.0*BL*Tc*Tm - 100000000.0*Bm*TL*Tc - 3000000000000.0*TL*Tc*Tm, 3.0*BL + 3.0*Bm - 10000.0*TL - 10000.0*Tm - 10000.0*BL*Bm*Tc - 100000000.0*BL*Tc*Tm - 100000000.0*Bm*TL*Tc + 3000000000000.0*TL*Tc*Tm, BL + Bm - 10000.0*TL - 10000.0*Tm - 10000.0*BL*Bm*Tc + 100000000.0*BL*Tc*Tm + 100000000.0*Bm*TL*Tc - 1000000000000.0*TL*Tc*Tm];
co_num =[100000000.0*TL*Tc + 10000.0*BL*Tc + 1.0, 10000.0*BL*Tc - 100000000.0*TL*Tc + 3.0, 3.0 - 10000.0*BL*Tc - 100000000.0*TL*Tc, 100000000.0*TL*Tc - 10000.0*BL*Tc + 1.0];
Gd=tf(co_num,co_den,Ts); 
out_target=lsim(Gd,u,t);

%如果有实测数据
Ts=200e-6;
u=I_feed;
out_target=v_feed;
t=0:200e-6:(length(I_give)-1)*200e-6;


%% init GA 
N=10000;%个体数
dim=5;%修改参数数目
lb = [0.01,0.005,0.05,0,0]; %上下界
ub = [0.05,0.03,2,0.0001,1e-5];
bound=[lb' ub'];

pm = 0.4;%变异概率
pc = 0.6;%交叉概率
P0=zeros(N,dim);
for i = 1:dim
P0(:,i) = bound(i,1)+rand(N,1)*(bound(i,2)-bound(i,1));%随机初始化
end
%% main GA
nloop=10;
for loops = 1:nloop%总循环周期数
for n = 1:N
    error_es(n) = mymodel(P0(n,1),P0(n,2),P0(n,3),P0(n,4),P0(n,5),u,t,out_target,Ts); %**修改函数输入量
%     emean(n) = mean(abs(error_es));
    fitness(n) =1/error_es(n);
%     fitness(n)=fitness(n)^0.1;
end
P_total{loops}=P0;
fitness_total{loops}=fitness;
[~,best_current_ind]=max(fitness);
sP = myselect(P0,fitness);
cP = mycross(sP,pc,N,1);% Nc为交叉次数,0相邻交叉，1随机交叉
bound_mutate = 0.02*bound;
mP = mymutate(cP,pm,N,bound_mutate,fitness);%突变范围 bound_mutate 可能超出变量限值
mP(1,:)=P0(best_current_ind,:); %本代赋给下一代初始值,本代最优的重复20次?
for i = 1:dim
mP(end-N*0.1+1:end,i) = bound(i,1)+rand(0.1*N,1)*(bound(i,2)-bound(i,1));%随机最后的百分之十避免早熟
end
P0=mP;
em(loops)=min(error_es);%记录每一代最小误差
% if em(loops)<0.01
%     continue
% end
end

%作图
figure(1)
plot(log(em),'o');
title('误差收敛曲线')

figure(2)
[e_best,ind_best]=min(em);%最小误差在那一代
Pbest=P_total{ind_best};
plot(Pbest)
ylabel('该代内所有个体的值')
title(['误差最小的是第' num2str(ind_best) '代']);

figure(3)
[fit_best,ind_fb]=max(fitness_total{ind_best});%找到最小误差代的最小误差个体
plot(fitness_total{ind_best})
title(['适应度最高的是第' num2str(ind_best) '代的第' num2str(ind_fb) '个体'])
% x_target=vpa([Tm TL Bm BL Tc])
x=vpa(Pbest(ind_fb,:))
x=eval(x);
figure(4)
Tm=x(1);
TL=x(2);
Bm=x(3);
BL=x(4);
Tc=x(5);
co_den =[BL + Bm + 10000.0*TL + 10000.0*Tm + 10000.0*BL*Bm*Tc + 100000000.0*BL*Tc*Tm + 100000000.0*Bm*TL*Tc + 1000000000000.0*TL*Tc*Tm, 3.0*BL + 3.0*Bm + 10000.0*TL + 10000.0*Tm + 10000.0*BL*Bm*Tc - 100000000.0*BL*Tc*Tm - 100000000.0*Bm*TL*Tc - 3000000000000.0*TL*Tc*Tm, 3.0*BL + 3.0*Bm - 10000.0*TL - 10000.0*Tm - 10000.0*BL*Bm*Tc - 100000000.0*BL*Tc*Tm - 100000000.0*Bm*TL*Tc + 3000000000000.0*TL*Tc*Tm, BL + Bm - 10000.0*TL - 10000.0*Tm - 10000.0*BL*Bm*Tc + 100000000.0*BL*Tc*Tm + 100000000.0*Bm*TL*Tc - 1000000000000.0*TL*Tc*Tm];
co_num =[100000000.0*TL*Tc + 10000.0*BL*Tc + 1.0, 10000.0*BL*Tc - 100000000.0*TL*Tc + 3.0, 3.0 - 10000.0*BL*Tc - 100000000.0*TL*Tc, 100000000.0*TL*Tc - 10000.0*BL*Tc + 1.0];
Gd=tf(co_num,co_den,Ts); 
out_sim=lsim(Gd,u,t);
plot(t,out_sim,t,out_target)
% 

%% 
function F = mymodel(A1,A2,A3,A4,A5,u,t,out_target,Ts)%**
Tm=A1;
TL=A2;
Bm=A3;
BL=A4;
Tc=A5;
% co_num = [TL*Tc, BL*Tc, 1];
% co_den = [TL*Tc*Tm, BL*Tc*Tm + Bm*TL*Tc, TL + Tm + BL*Bm*Tc, BL + Bm];
% Gs=tf(co_num,co_den);
% Gd=c2d(Gs,200e-6);
co_den =[BL + Bm + 10000.0*TL + 10000.0*Tm + 10000.0*BL*Bm*Tc + 100000000.0*BL*Tc*Tm + 100000000.0*Bm*TL*Tc + 1000000000000.0*TL*Tc*Tm, 3.0*BL + 3.0*Bm + 10000.0*TL + 10000.0*Tm + 10000.0*BL*Bm*Tc - 100000000.0*BL*Tc*Tm - 100000000.0*Bm*TL*Tc - 3000000000000.0*TL*Tc*Tm, 3.0*BL + 3.0*Bm - 10000.0*TL - 10000.0*Tm - 10000.0*BL*Bm*Tc - 100000000.0*BL*Tc*Tm - 100000000.0*Bm*TL*Tc + 3000000000000.0*TL*Tc*Tm, BL + Bm - 10000.0*TL - 10000.0*Tm - 10000.0*BL*Bm*Tc + 100000000.0*BL*Tc*Tm + 100000000.0*Bm*TL*Tc - 1000000000000.0*TL*Tc*Tm];
co_num =[100000000.0*TL*Tc + 10000.0*BL*Tc + 1.0, 10000.0*BL*Tc - 100000000.0*TL*Tc + 3.0, 3.0 - 10000.0*BL*Tc - 100000000.0*TL*Tc, 100000000.0*TL*Tc - 10000.0*BL*Tc + 1.0];
Gd=tf(co_num,co_den,Ts); 
out_sim=lsim(Gd,u,t);
F=var(out_sim-out_target);
% F=sqrt(mean((out_sim-out_target).^2));
end
function sP = myselect(P0,fitness)
%轮盘赌算法 即将所有个体的概率相加分段归一化，随机落在某一段。落在该段概率与该段所属个体fitness成正比
N = size(fitness,2);
sump=[fitness(1)/sum(fitness)];
for n = 2:N
    p=fitness(n)/sum(fitness);%该个体被选中的概率
    sump(n)=p+sump(n-1);%该段概率和
end
for n = 1: N
    index=find(sump-rand>0);
    index=index(1);
    sP(n,:)=P0(index,:);
end
end

function cP = mycross(sP,pc,N,fcn)
%任取两个进行交叉
cP=sP;
k=0.5;%交叉权重\
if fcn == 0
    for n=1:2:N %相邻交叉
        if rand<pc && n~=N
            cP(n,:)=k.*sP(n,:)+(1-k).*sP(n+1,:);
            cP(n+1,:)=k.*sP(n+1,:)+(1-k).*sP(n,:);
        end
    end
elseif fcn == 1
    for n=1:N %随机交叉
        if rand<pc
            pick=randi(N,1,2);
            n1=pick(1);
            n2=pick(2);
            cP(n1,:)=k.*sP(n1,:)+(1-k).*sP(n2,:);
            cP(n2,:)=k.*sP(n2,:)+(1-k).*sP(n1,:);
        end
    end
end
end

function mP = mymutate(cP,pm,N,bound_mutate,fitness) %cP是交叉后的个体
mP=cP;
bm=bound_mutate;
    for n = 1:N
        if rand<pm %&& fitness(n)<=mean(fitness) %判断是否变异 %仅把得分低的进行变异
            mP(n,:)=cP(n,:)+(bm(:,1)+(bm(:,2)-bm(:,1))*rand)'; %变异就是在原来的基础上加一个变异边界范围内的随机值
        end
    end
end