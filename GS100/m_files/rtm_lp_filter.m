%给定采样频率，lowpass频率 在lp处衰减-3db 获得Z域的离散滤波器 num den系数
function [n_cal,d_cal]=rtm_lp_filter(fs,fp)


num_coeff= [5000 - 5000*cos((2*pi*fp)/fs), 10000 - 10000*cos((2*pi*fp)/fs), 5000 - 5000*cos((2*pi*fp)/fs)];
num_coeff=vpa(num_coeff,4);
n_cal = double(num_coeff);
n_cal=flip(n_cal);%原本是从高次到低次

den_coeff= [10000 - 7071*sin((2*pi*fp)/fs), -20000*cos((2*pi*fp)/fs), 7071*sin((2*pi*fp)/fs) + 10000];
den_coeff=vpa(den_coeff,4);
d_cal=double(den_coeff);
d_cal=flip(d_cal);

[h,w]=freqz(n_cal,d_cal,round(fs/2),fs);
plot(w,abs(h));grid on;

end