syms ki k1 k2 k3 s %Bm kp Tm TL Tc Dp1 Dp2 wp1 wp2
Dp1=0.9;
Dp2=0.9;
wp1=2*pi*300;
wp2=2*pi*300;
kp=ki/wp2;
sp1=wp1*(-Dp1+sqrt(1-Dp1^2)*1i);
sp2=wp1*(-Dp1-sqrt(1-Dp1^2)*1i);
sp3=wp2*(-Dp2+sqrt(1-Dp2^2)*1i);
sp4=wp2*(-Dp2-sqrt(1-Dp2^2)*1i);
P=(s-sp1)*(s-sp2)*(s-sp3)*(s-sp4);
P=expand(P);
P=collect(P,s);
coef_set=coeffs(P,s);
% A=[(-Bm+k1-kp)/Tm  (k2-1)/Tm    k3/Tm    ki/Tm;
%     1/Tc             0         -1/Tc      0;
%     0                1/TL        0        0;
%     -1               0           0        0];
% B=[kp/Tm 0 0 1]';
% C=[1 0 0 0];
% D=0;
% Gs=C*inv(s*eye(4)-A)*B
% Gs=(TL*Tc*kp*s^3 + TL*Tc*ki*s^2 + kp*s + ki)/(TL*Tc*Tm*s^4 + (TL*Tc*kp - TL*Tc*k1 + Bm*TL*Tc)*s^3 + (TL + Tm - TL*k2 + TL*Tc*ki)*s^2 + (kp - k1 - k3 + Bm)*s + ki)
% dems=s^4 + (TL*Tc*kp - TL*Tc*k1 + Bm*TL*Tc)/(TL*Tc*Tm)*s^3 + (TL + Tm - TL*k2 + TL*Tc*ki)/(TL*Tc*Tm)*s^2 + (kp - k1 - k3 + Bm)/(TL*Tc*Tm)*s + ki/(TL*Tc*Tm)
coef_model=[ki/(TL*Tc*Tm),(kp - k1 - k3 + Bm)/(TL*Tc*Tm),(TL + Tm - TL*k2 + TL*Tc*ki)/(TL*Tc*Tm),(TL*Tc*kp - TL*Tc*k1 + Bm*TL*Tc)/(TL*Tc*Tm),1];
[a,b,c,d]=solve(coef_set==coef_model,[ki,k1,k2,k3]);
ki=eval(a);
k1=eval(b);
k2=eval(c);
k3=eval(d);
kp=ki/wp2;
Kp_vel_ob=kp;
Ki_vel_ob=ki;


