clear 
clc
data = csvread('2021_8_25_10_17_16.csv',1,0);
V = data(:,2);
Iq_feed=data(:,3);

fs=333;
fs1=[9,75,100,150,155];%set band stop filter start freq
fs2=[11,78,110,155,165];% band stop filter stop freq
% [n_cal,d_cal]=rtm_bs_filter(fs,fs1,fs2);
% Iq_feed_filter=filter(n_cal,d_cal,Iq_feed);
temp=Iq_feed;
for i=1:length(fs1)
    [n_cal,d_cal]=rtm_bs_filter(fs,fs1(i),fs2(i));
    Iq_feed_filter=filter(n_cal,d_cal,temp);
    temp=Iq_feed_filter;
end

figure
% a = figure('name','original vs filtered');
L=length(Iq_feed);
Origin_ff = fft(Iq_feed);
Filtered_ff =fft(Iq_feed_filter);
P2 = abs(Origin_ff/L);
feed_P2 = abs(Filtered_ff/L);
P1 = P2(1:L/2);
feed_P1 = feed_P2(1:L/2);
P1(2:end-1) = 2*P1(2:end-1);
feed_P1(2:end-1) = 2*feed_P1(2:end-1);
f = fs*(0:(L-1))'/L;
plot(f(1:size(P1)),P1);hold on;plot(f(1:size(feed_P1)),feed_P1)
legend('original','filtered')
% freqz(n_cal,d_cal,2560,fs)
figure
plot(Iq_feed_filter)
hold on;plot(Iq_feed)
