clear;
syms L1 L2 L3 L4 
syms Xp Xi
%motor parameter 
Ia=2.90;
Rs=6.06/2;
Ls=(16.8/3)*1e-3;
R=1.18;%R>4 ,状态反馈+离散观测器仿真会震荡
J1=0.437*1e-4;
J2=J1*R;
Kt=0.239;
poles=5;
Ct=0;%stiff
Dt=0;
Mb=1.27;
K=100;%NM/rad
%quantization
Ub=180;
Pb=400;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
Ib=Ia*sqrt(2);
Zb=Ub/Ib;
Ob=100*pi;
Wb=Ob*poles;
Fb=Ub/Wb;
Lb=Fb/Ib;
Jb=Mb/Ob;

Ks=K/Jb;%刚度系数标幺
Rm=Rs/Zb;
Lm=Ls/Zb;
d=(Dt*Ob)/Mb;%Dt=0
Tm=J1/Jb;
TL=J2/Jb; 
Tc=1/Ks;